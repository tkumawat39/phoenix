<?php

namespace App\Repository;

use App\Entity\LeadsCstm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LeadsCstm|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeadsCstm|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeadsCstm[]    findAll()
 * @method LeadsCstm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadsCstmRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LeadsCstm::class);
    }

    // /**
    //  * @return LeadsCstm[] Returns an array of LeadsCstm objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeadsCstm
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    //-------------------------------------------------------------------------

    /**
     * Checks if present address of the passed lead is less than one year.
     *
     * @param int $leadId
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function presentAddressLessThanOneYear($leadId)
    {
        $query = $this->createQueryBuilder('lcstm')
                    ->where('lcstm.id_c = :leadId')
                    ->andWhere('lcstm.borrower_address_no_yrs_c < 1')
                    ->setParameter('leadId', $leadId)
                    ->getQuery();

        $product = $query->setMaxResults(1)->getOneOrNullResult();

        if( ! $product)
        {
            return false;
        }

        return true;
    }
}
// end of class
// end of file