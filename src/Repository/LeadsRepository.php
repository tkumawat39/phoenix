<?php

namespace App\Repository;

use App\Entity\Leads;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Leads|null find($id, $lockMode = null, $lockVersion = null)
 * @method Leads|null findOneBy(array $criteria, array $orderBy = null)
 * @method Leads[]    findAll()
 * @method Leads[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Leads::class);
    }

    // /**
    //  * @return Leads[] Returns an array of Leads objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Leads
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    //-------------------------------------------------------------------------

    /**
     * Returns the leads stored in the applicaiton.
     *
     * @param null $offset
     * @param null $limit
     * @return mixed
     */
    public function listLeads($offset = null, $limit = null)
    {
        // calculating the value of the offset
        if( !is_null($offset) && !is_null($limit))
        {
            $offset = ( $offset - 1 ) * $limit + 1;
        }


        $query = $this->createQueryBuilder('l')
            ->orderBy('l.date_entered', 'desc');

        // setting offset
        if( !is_null($offset))
        {
            $query->setFirstResult($offset);
        }

        //setting limit
        if( !is_null($limit))
        {
            $query->setMaxResults($limit);
        }

        return $query->getQuery()->getArrayResult();

    }

}
// end of class
// end of file