<?php


namespace App\Service\SugarCRM;

use App\Entity\Leads;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LeadCRUD
 *
 * @package App\Service\SugarCRM
 */
class LeadCRUD
{

    /**
     * @var
     */
    protected $entityManager;

    //-------------------------------------------------------------------------

    /**
     * LeadCRUD constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    //-------------------------------------------------------------------------


    /**
     * Update existing resource matching passed id.
     *
     * @param string $id
     * @param array $data
     * @return int
     * @throws \Exception
     */
    public function update($id, $data)
    {
        // reading the lead object
        $lead = $this->entityManager->getRepository(Leads::class)
                    ->find($id);

        if(!$lead)
        {
            // Todo: Handle exception here
            return Response::HTTP_NOT_FOUND;
        }

        //initializing the information to be update
        $lead->setDescription($data['description']);
        $lead->setSalutation($data['salutation']);
        $lead->setFirstName($data['firstName']);
        $lead->setLastName($data['lastName']);
        $lead->setTitle($data['title']);
        $lead->setFacebook($data['facebook']);
        $lead->setTwitter($data['twitter']);
        $lead->setGoogleplus($data['googleplus']);
        $lead->setDepartment($data['department']);
        $lead->setDoNotCall($data['doNotCall']);
        $lead->setPhoneHome($data['phoneHome']);
        $lead->setPhoneMobile($data['phoneMobile']);
        $lead->setPhoneWork($data['phoneWork']);
        $lead->setPhoneOther($data['phoneOther']);
        $lead->setPhoneFax($data['phoneFax']);
        $lead->setPrimaryAddressStreet($data['primaryAddressStreet']);
        $lead->setPrimaryAddressCity($data['primaryAddressCity']);
        $lead->setPrimaryAddressState($data['primaryAddressState']);
        $lead->setPrimaryAddressPostalcode($data['primaryAddressPostalcode']);
        $lead->setPrimaryAddressCountry($data['primaryAddressCountry']);
        $lead->setAltAddressStreet($data['altAddressStreet']);
        $lead->setAltAddressCity($data['altAddressCity']);
        $lead->setAltAddressState($data['altAddressState']);
        $lead->setAltAddressPostalcode($data['altAddressPostalcode']);
        $lead->setAltAddressCountry($data['altAddressCountry']);
        $lead->setAssistant($data['assistant']);
        $lead->setAssistantPhone($data['assistantPhone']);
        $lead->setPicture($data['picture']);
        $lead->setConverted($data['converted']);
        $lead->setReferedBy($data['referedBy']);
        $lead->setLeadSource($data['leadSource']);
        $lead->setLeadSourceDescription($data['leadSourceDescription']);
        $lead->setStatus($data['status']);
        $lead->setStatusDescription($data['statusDescription']);
        $lead->setReportsToId($data['reportsToId']);
        $lead->setDnbPrincipalId($data['dnbPrincipalId']);
        $lead->setAccountName($data['accountName']);
        $lead->setContactId($data['contactId']);
        $lead->setAccountId($data['accountId']);
        $lead->setOpportunityId($data['opportunityId']);
        $lead->setOpportunityName($data['opportunityName']);
        $lead->setOpportunityAmount($data['opportunityAmount']);
        $lead->setCampaignId($data['campaignId']);
        $lead->setBirthdate($data['birthdate']);
        $lead->setPortalName($data['portalName']);
        $lead->setPortalApp($data['portalApp']);
        $lead->setWebsite($data['website']);
        $lead->setPreferredLanguage($data['preferredLanguage']);
        $lead->setMktoSync($data['mkToSync']);
        $lead->setMktoId($data['mkToId']);
        $lead->setMktoLeadScore($data['mkToLeadScore']);
        $lead->setAssignedUserId($data['assignedUserId']);
        $lead->setTeamId($data['teamId']);
        $lead->setTeamSetId($data['teamSetId']);
        $lead->setAclTeamSetId($data['aclTeamSetId']);
        $lead->setDriWorkflowTemplateId($data['driWorkflowTemplateId']);
        $lead->setDateModified(new \DateTime());

        // writing the updated information in the database
        $this->entityManager->flush();

        return Response::HTTP_OK;
    }
}
//end of class
// end of file