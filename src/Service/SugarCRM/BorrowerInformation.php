<?php
namespace App\Service\SugarCRM;


use App\Service\CustomRequest;

/**
 * Class BorrowerInformation
 *
 * @package App\Service\SugarCRM
 * @since 1.0.0
 * @version 1.0.0
 */
class BorrowerInformation
{

    /**
     * Instance of CustomRequest
     * 
     * @var CustomRequest
     */
    protected $customRequest;

    //-------------------------------------------------------------------------

    /**
     * BorrowerInformation constructor.
     *
     * @param CustomRequest $customRequest
     */
    public function __construct(CustomRequest $customRequest)
    {
        $this->customRequest = $customRequest;
    }

    //-------------------------------------------------------------------------

    /**
     * Returns the borrower information from the SugarCRM.
     *
     * @param mixed $customerId
     * @return array
     */
    public function pullCustomerInformation($customerId)
    {
        //$this->customRequest->sendGetRequest('some_url');
        return $this->formatCustomerInformation();
    }

    //-------------------------------------------------------------------------

    /**
     * Formats the response received from the sugar crm.
     *
     * @return array
     */
    private function formatCustomerInformation()
    {
        return $data = [
            'borrower' => [
                "firstname" => "Stephanie",
                "lastname" => "Brown",
                "email" => "stephbrown6714@gmail.com",
                "mobile" => "(214) 560-0209",
                "ssn" => "",
                "dob" => "2019-06-18",
                "subjectPropertyAddressState" => "",
                "address" => [
                    "present" => [
                        "street" => "14277, Preston Road",
                        "city" => "Presenton",
                        "state" => "",
                        "postalCode" => "75240",
                        "rentOrOwner" => "",
                        "durationOfStay" => ""
                    ],
                    "former" => [
                        "street" => "13150 Coit Road",
                        "city" => "Dallas",
                        "state" => "",
                        "postalCode" => "75240",
                        "rentOrOwner" => "",
                        "durationOfStay" => ""
                    ]
                ]
            ],
            "coborrower" => [
                "firstname" => "Stephanie",
                "lastname" => "Brown",
                "email" => "stephbrown6714@gmail.com",
                "mobile" => "(214) 560-0209",
                "ssn" => "",
                "dob" => "2019-06-18",
                "address" => [
                    "present" => [
                        "street" => "14277, Preston Road",
                        "city" => "Presenton",
                        "state" => "",
                        "postalCode" => "75240"
                    ],
                    "former" => [
                        "street" => "1350 Coit Road",
                        "city" => "Dallas",
                        "state" => "",
                        "postalCode" => "75240",
                        "rentOrOwner" => "",
                        "durationOfStay" => ""
                    ]
                ]
             ]
        ];
    }

}
// end of class
// end of file