<?php


namespace App\Service\SugarCRM;

use App\Service\CustomRequest;
use App\Entity\Leads as LeadEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Leads
 *
 * @package App\Service\SugarCRM
 * @since 1.0.0
 * @version 1.0.0
 * @author Parth Shukla <parthshukla@ahex.co.in>
 */
class Leads
{

    /**
     * Instance of the CustomRequest
     *
     * @var CustomRequest
     */
    protected $customRequest;


    protected $entityManager;


    protected $leadEntity;



    //-------------------------------------------------------------------------

    /**
     * Leads constructor.
     *
     * @param CustomRequest $customRequest
     */
    public function __construct(CustomRequest $customRequest,
                                EntityManagerInterface $entityManager
                                //LeadEntity $leadEntity
                            )
    {
        $this->customRequest = $customRequest;
        $this->entityManager = $entityManager;
        //$this->leadEntity = $leadEntity;
    }

    //-------------------------------------------------------------------------

    /**
     * Sends request to sugarcrm to get the list of leads.
     *
     * @return array
     */
    public function getLeadsList()
    {
        $data = $this->customRequest->sendGetRequest('my-url');

        return $this->processLeadListResponse([]);

    }

    //-------------------------------------------------------------------------

    /**
     * Formats the response received from the CRM/
     *
     * @param $data
     * @return array
     */
    private function processLeadListResponse($data)
    {
        return $data = [
                "leads" => [
                    [
                        "firstName" => "Charoltte",
                        "lastName" => "Wright",
                        "leadMedium" => "Website",
                        "loanPurpose" => "Refinancing",
                        "leadSource" => "Contact Form",
                        "dateReceived" => "03/30/2018 | 04:01 PM",
                        "loanAmount" => "285000",
                        "assignedAgent" => "Alice Sample-Tester",
                        "salesStage" => "Uncontacted",
                        "state" => "TX"
                    ],
                    [
                        "firstName" => "Mary",
                        "lastName" => "Higgins",
                        "leadMedium" => "Paid Lead Source 2",
                        "loanPurpose" => "Refinancing",
                        "leadSource" => "Abandoned App",
                        "dateReceived" => "05/07/2018 | 03:00 AM",
                        "loanAmount" => "320000",
                        "assignedAgent" => "Brian Apunda",
                        "salesStage" => "Uncontacted",
                        "state" => "AL"
                    ],
                    [
                        "firstName" => "Greg",
                        "lastName" => "Abbot",
                        "leadMedium" => "Website",
                        "loanPurpose" => "Refinancing",
                        "leadSource" => "Contact Form",
                        "dateReceived" => "03/30/2018 | 04:01 PM",
                        "loanAmount" => "285000",
                        "assignedAgent" => "Alice Sample-Tester",
                        "salesStage" => "Uncontacted",
                        "state" => "TX"
                    ],
                    [
                        "firstName" => "Kyle",
                        "lastName" => "Brower",
                        "leadMedium" => "Branch Boost",
                        "loanPurpose" => "Refinancing",
                        "leadSource" => "Contact Form",
                        "dateReceived" => "05/13/2018 | 02:32 PM",
                        "loanAmount" => "180000",
                        "assignedAgent" => "Brian Apunda",
                        "salesStage" => "Uncontacted",
                        "state" => "AL"
                    ],
                    [
                        "firstName" => "Judith",
                        "lastName" => "Dignan",
                        "leadMedium" => "Website",
                        "loanPurpose" => "Purchasing",
                        "leadSource" => "Miscellaneous",
                        "dateReceived" => "05/23/2018 | 01:47 PM",
                        "loanAmount" => "225000",
                        "assignedAgent" => "Jeff Brown",
                        "salesStage" => "Uncontacted",
                        "state" => "MO"
                    ],
                    [
                        "firstName" => "Craig",
                        "lastName" => "Elroy",
                        "leadMedium" => "Website",
                        "loanPurpose" => "Purchasing",
                        "leadSource" => "Contact Form",
                        "dateReceived" => "05/30/2018 | 08:26 AM",
                        "loanAmount" => "120000",
                        "assignedAgent" => "Alice Sample-Tester",
                        "salesStage" => "Uncontacted",
                        "state" => "TX"
                    ],
                    [
                        "firstName" => "Franklin",
                        "lastName" => "Wright",
                        "leadMedium" => "Paid Lead Source 1",
                        "loanPurpose" => "Purchasing",
                        "leadSource" => "Contact Form",
                        "dateReceived" => "06/03/2018 | 11:50 AM",
                        "loanAmount" => "320000",
                        "assignedAgent" => "Alice Sample-Tester",
                        "salesStage" => "Uncontacted",
                        "state" => "TX"
                    ]
                ]
            ];

    }

    //-------------------------------------------------------------------------

    /**
     * Updates the lead matching the passed id.
     *
     * @param integer $id
     * @param array $data
     * @return int
     */
    public function update($id, $data)
    {
        // reading the lead object
        $lead = $this->entityManager->getRepository(LeadEntity::class)->find($id);

        if(!$lead)
        {
            // To-Do: Handle exception here
            return Response::HTTP_NOT_FOUND;
        }

        //initializing the information to be update
        $lead->setSalutation($data['salutation']);
        $lead->setFirstName($data['firstName']);
        $lead->setLastName($data['lastName']);
        $lead->setTitle($data['title']);
        $lead->setFacebook($data['facebook']);
        $lead->setTwitter($data['twitter']);
        $lead->setGoogleplus($data['googlePlus']);
        $lead->setDepartment($data['department']);
        $lead->setDoNotCall($data['doNotCall']);
        $lead->setPhoneHome($data['phoneHome']);
        $lead->setPhoneMobile($data['phoneMobile']);
        $lead->setPhoneWork($data['phoneWork']);
        $lead->setPhoneOther($data['phoneOther']);
        $lead->setPhoneFax($data['phoneFax']);
        $lead->setPrimaryAddressStreet($data['primaryAddressStreet']);
        $lead->setPrimaryAddressCity($data['primaryAddressCity']);
        $lead->setPrimaryAddressState($data['primaryAddressState']);
        $lead->setPrimaryAddressPostalcode($data['primaryAddressPostalCode']);
        $lead->setPrimaryAddressCountry($data['country']);
        $lead->setAltAddressStreet($data['altAddressStreet']);
        $lead->setAltAddressCity($data['altAddressCity']);
        $lead->setAltAddressState($data['altAddressState']);
        $lead->setAltAddressPostalcode($data['altAddressPostalCode']);
        $lead->setAltAddressCountry($data['altAddressCountry']);
        $lead->setAssistant($data['assistant']);
        $lead->setAssistantPhone($data['assistantPhone']);
        $lead->setPicture($data['picture']);
        $lead->setConverted($data['converted']);
        $lead->setReferedBy($data['referedBy']);
        $lead->setLeadSource($data['leadSource']);
        $lead->setLeadSourceDescription($data['leadSourceDescription']);
        $lead->setStatus($data['status']);
        $lead->setStatusDescription($data['statusDescription']);
        $lead->setReportsToId($data['reportsToId']);
        $lead->setDnbPrincipalId($data['dnbPrincipalId']);
        $lead->setAccountName($data['accountName']);
        $lead->setContactId($data['contactId']);
        $lead->setAccountId($data['accountId']);
        $lead->setOpportunityId($data['opportunityId']);
        $lead->setOpportunityName($data['opportunityName']);
        $lead->setOpportunityAmount($data['opportunityAmount']);
        $lead->setCampaignId($data['campaignId']);
        $lead->setBirthdate($data['birthdate']);
        $lead->setPortalName($data['portalName']);
        $lead->setPortalApp($data['portalApp']);
        $lead->setWebsite($data['website']);
        $lead->setPreferredLanguage($data['preferredLanguage']);
        $lead->setMktoSync($data['mkToSync']);
        $lead->setMktoId($data['mkToId']);
        $lead->setMktoLeadScore($data['mkToLeadScore']);
        $lead->setAssignedUserId($data['assignedId']);
        $lead->setTeamId($data['teamId']);
        $lead->setTeamSetId($data['teamSetId']);
        $lead->setAclTeamSetId($data['aclTeamSetId']);
        $lead->setDriWorkflowTemplateId($data['driWorkflowTemplateId']);
        $lead->setDateModified(date('Y-m-d H:i:s'));

        // writing the updated information in the database
        $this->entityManager->flush();

        return Response::HTTP_OK;
    }

}
// end of class
// end of file