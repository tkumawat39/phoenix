<?php
namespace App\Service\Calyx;

use App\Entity\Leads as LeadEntity;
use App\Service\CustomRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ImportCustomerInformation
 *
 * @package App\Service\Calyx
 * @since 1.0.0
 * @version 1.0.0
 * @author Parth Shukla <parthshukla@ahex.co.in>
 */
class ImportCustomerInformation
{

    /**
     * Instance
     *
     * @var CustomRequest
     */
    protected $customRequest;

    /**
     * Instance
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;

    //-------------------------------------------------------------------------

    /**
     * ImportCustomerInformation constructor.
     *
     * @param CustomRequest $customRequest
     */
    public function __construct(CustomRequest $customRequest,
                                EntityManagerInterface $entityManager
                                )
    {
        $this->customRequest = $customRequest;
        $this->entityManager = $entityManager;
    }

    //-------------------------------------------------------------------------

    /**
     * Sends the import request to calyx point for saving borrower information.
     *
     * @param $data
     * @return bool|string
     */
    public function sendImportRequest($data)
    {
        $data = $this->setImportRequestPayload($data);
        $responseData = $this->customRequest->sendPostRequest($_ENV['CALYX_POINT_URL'], $data);

        $response = json_decode($responseData);

        //updating the user loan journey account number
        $this->updateLoanNumber('4366ff70-91f5-11e9-beb2-cd60ed7c4f7e',
                                $response->Data->Info->Attributes->FileName
                            );

        return new JsonResponse($response, Response::HTTP_CREATED);
    }

    //-------------------------------------------------------------------------

    /**
     * Formats the user submitted data to the structure required by the
     * calyx point.
     *
     * @param $data
     * @return array
     */
    private function setImportRequestPayload($data)
    {
        // adding additional fields in the data to be sent
        $data['Username'] = $_ENV['CALYX_POINT_USERNAME'];
        $data['Password'] = $_ENV['CALYX_POINT_PASSWORD'];
        $data['dataFolderName'] = $_ENV['CALYX_POINT_DATA_FOLDER_NAME'];
        $data['searchParameter'] = "";
        $data['searchPatternEx'] = "";
        $data['overWrite'] = "false";

        return $data;
    }

    //-------------------------------------------------------------------------

    /**
     * Updates the loan number into the sugar crm database.
     *
     * @param $id
     * @param $loanNumber
     * @return int
     * @throws \Exception
     */
    protected function updateLoanNumber($id, $loanNumber)
    {
        // reading the lead object
       /* $lead = $this->entityManager->getRepository(LeadEntity::class)->find($id);

        if(!$lead)
        {
            // To-Do: Handle exception here
            return Response::HTTP_NOT_FOUND;
        }

        //initializing the information to be updated
        $lead->setAccountId($loanNumber);
        $lead->setDateModified(new \DateTime());
        $lead->setDeleted(0);

        // writing the updated information in the database
        $this->entityManager->flush();*/

        return Response::HTTP_OK;
    }
}
// end of class
// end of file
