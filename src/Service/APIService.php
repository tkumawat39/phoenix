<?php


namespace App\Service;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class APIService
 *
 * @package App\Service
 */
abstract class APIService
{
    /**
     * Status code for the response to be sent.
     *
     * @var int
     */
    protected $statusCode;

    /**
     * Array to store the data to be returned in the response for
     * the requests.
     *
     * @var array
     */
    protected $response = [];

    //-------------------------------------------------------------------------

    /**
     * Sets the status code.
     *
     * @param int $code
     * @return $this
     */
    public function setStatusCode(int $code)
    {
        $this->statusCode = $code;

        return $this;
    }

    //-------------------------------------------------------------------------

    /**
     * Returns sthe status code.
     *
     * @return int
     */
    public function getStatusCode() : int
    {
        return $this->statusCode;
    }

    //-------------------------------------------------------------------------

    /**
     * @param array $data
     * @param array $headers
     * @return Response
     */
    protected function respond(array $data, array $headers=[])
    {
        return new Response(
            json_encode($data),
            $this->statusCode,
            $headers
        );
    }

    //-------------------------------------------------------------------------

    /**
     * @param string $message
     * @param array $data
     * @return Response
     */
    public function getResponse($message = '', $data = [])
    {
        return $this->respond(['data' => $data]);
    }

    //-------------------------------------------------------------------------

    /**
     * Method to return the response when resource not found.
     *
     * @param String $message
     * @return Response
     */
    public function responseWithNotFound($message = "Not Found!")
    {
        return $this->setStatusCode(Response::HTTP_NOT_FOUND)->getResponse($message);
    }

    //-------------------------------------------------------------------------

    /**
     * Method to return the response when internal server error occurred.
     *
     * @param string $message
     * @return Response
     */
    public function responseWithInternalServerError($message = 'Server Error!')
    {
        return $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)->getResponse($message);
    }

    //-------------------------------------------------------------------------

    /**
     * Method to return the response when user is not authorized to perform the
     * requested action.
     *
     * @param string $message
     * @return Response
     */
    public function responseWithUnAuthorized($message = "You are not authorized to perform the requested action!")
    {
        return $this->setStatusCode(Response::HTTP_UNAUTHORIZED)->getResponse($message);
    }

    //-------------------------------------------------------------------------

    /**
     * Method to return the resopnse when the request raised by the user
     * has been processed successfully.
     *
     * @param $message
     * @param array $data
     * @return mixed
     */
    public function responseWithSuccess($message = 'Success', $data=[])
    {
        return $this->setStatusCode(Response::HTTP_OK)->getResponse($message, $data);
    }

    //-------------------------------------------------------------------------

    /**
     * Method to return response when the resource has been successfully created.
     *
     * @param string $message
     * @param array $data
     * @return Response
     */
    public function responseWithSuccessfullyCreated($message = 'Resource created successfully.',
                                                    $data = [])
    {
        return $this->setStatusCode(Response::HTTP_CREATED)->getResponse($message, $data);
    }

    //-------------------------------------------------------------------------

    /**
     * Method to return response when the request has been successfully accepted.
     *
     * @param string $message
     * @param array $data
     * @return Response
     */
    public function responseWithSuccessfullyAccepted($message = 'Request accepted successfully.',
                                                    $data = [])
    {
        return $this->setStatusCode(Response::HTTP_ACCEPTED)->getResponse($message, $data);
    }
}
// end of class
// end of file