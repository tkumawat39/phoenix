<?php
namespace App\Service;

/**
 * Class CustomRequest
 *
 * @package App\Service
 * @since 1.0.0
 * @version 1.0.0
 * @author Parth Shukla <parthshukla@ahex.co.in>
 */
class CustomRequest
{

    /**
     * Sends post request to passed url.
     *
     * @param string $url
     * @param array $data
     * @return bool|string
     */
    public function sendPostRequest( $url, $data)
    {
        // converting data to json format
        $payload = json_encode($data);

        // setting up request resource
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch,  CURLOPT_RETURNTRANSFER, 1);

        // sending the request
        $result = curl_exec($ch);

        //closing the channel
        curl_close($ch);

        return $result;
    }

    //-------------------------------------------------------------------------

    /**
     * Sends GET request to passed url.
     *
     * @param string $url
     * @return bool|string
     */
    public function sendGetRequest($url)
    {
        // setting up request resource
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // sending the request
        $result = curl_exec($ch);

        return $result;
    }

}
// end of class
// end of file