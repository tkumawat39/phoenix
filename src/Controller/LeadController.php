<?php

namespace App\Controller;

use App\Entity\Leads as LeadEntity;
use App\Service\SugarCRM\Leads;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Lead controller.
 * @Route("/api", name="api_")
 */

class LeadController extends FOSRestController
{
    /**
     * Instance of Leads
     *
     * @var Leads
     */
    protected $leads;

    /**
     * Borrower constructor.
     *
     * @param leads $leads
     */

    public function __construct(Leads $leads)
    {
        $this->leads = $leads;
    }

    /**
     * @Rest\Get("/getleads", name="getLeadsList")
     */
    public function getLeadList()
    {
        return $this->leads->getLeadsList();
    }

    /**
     * @Rest\Post("/addleads", name="addLead")
     */
    public function addLead(ValidatorInterface $validator, Request $request, SerializerInterface $serializer)
    {

        // Create a Lead instance from JSON
        $lead = $serializer->deserialize($request->getContent(), LeadEntity::class, 'json');
        $errors = $validator->validate($lead);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
            return ['error' => $errorsString];
        }

        // write code here to insert data in database table
        return ['success' => 'Lead added Successfully!'];
    }
}
