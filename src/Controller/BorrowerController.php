<?php

namespace App\Controller;

use App\Service\Calyx\ImportCustomerInformation;
use App\Service\SugarCRM\BorrowerInformation;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Borrower controller.
 * @Route("/api", name="api_")
 */
class BorrowerController extends FOSRestController
{

    /**
     * Instance of BorrowerInformation
     *
     * @var BorrowerInformation
     */
    protected $borrowerInformation;

    /**
     * Instance of ImportCustomerInformation
     *
     * @var ImportCustomerInformation
     */
    protected $importCustomerInformation;

    //-------------------------------------------------------------------------

    /**
     * Borrower constructor.
     *
     * @param BorrowerInformation $borrowerInformation
     * @param ImportCustomerInformation $importCustomerInformation
     */
    public function __construct(BorrowerInformation $borrowerInformation, ImportCustomerInformation $importCustomerInformation)
    {
        $this->borrowerInformation = $borrowerInformation;
        $this->importCustomerInformation = $importCustomerInformation;
    }

    /**
     * @Rest\Get("/borrower/{customerId}", name="get_borrower_by_Id")
     */
    public function getBorrowerById($customerId = 1)
    {
        return $this->borrowerInformation->pullCustomerInformation($customerId);
    }

    /**
     * @Rest\Post("/import-borrower-to-calyx-point", name="import_borrower_to_calyx_point")
     */
    public function postImportBorrowerToCalyxPoint(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        return $this->importCustomerInformation->sendImportRequest($data);


//        $randnum = rand(1111111111, 9999999999);
//        return $this->handleView($this->view(['status' => 'ok', 'Journey Loan Number' => $randnum]));
    }
}
