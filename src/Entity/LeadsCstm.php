<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LeadsCstmRepository")
 */
class LeadsCstm
{

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $id_c;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : "purchasing_home"})
     */
    private $looking_into_c = "purchasing_home";

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $lead_type_c;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=8, options={"default": 0.00000000})
     */
    private $ms_lat_c = 0.00000000;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=8, options={"default": 0.00000000})
     */
    private $ms_lng_c = 0.00000000;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ms_last_geocoded_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $geocode_state_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $originator_email_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $originator_name_c;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $claimed_c = 0;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $lead_outcome_c;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $check_back_later_date_c;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $did_not_convert_notes_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $loan_number_c;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lead_expiration_c;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $refinancing_trigger_c;

    /**
     * @ORM\Column(type="string", length=100, options={"default": "Low Credit Score"})
     */
    private $did_not_convert_reason_c = "Low Credit Score";

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_closed_c;

    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $re_assigned_c = 0;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $inbox25_score_c = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nmls_c;

    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $opt_in_c = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $originator_id_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $previous_agents_c;

    /**
     * @ORM\Column(type="smallint" options={"default": 0})
     */
    private $notify_agent_c = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marketing_campaign_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marketing_source_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marketing_medium_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marketing_content_c;

    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $corporate_lead_c = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $refinance_loan_id_c;

    /**
     * @ORM\Column(type="string", length=55, nullable=true)
     */
    private $previous_originator_nmls_c;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $manual_originator_c = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $previous_agents_history_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crmsug_4_originator_name_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crmsug_4_from_agent_name_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crmsug_4_originator_nmls_id_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crmsug_4_originator_address_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sugarcrm_4_originator_phone_c;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $crmsug_4_originator_title_c;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $call_or_note_c = 0;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $borrower_currently_rent_c = 0;

    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $borrower_currently_own_c = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $borrower_address_no_yrs_c;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $borrower_former_street_addre_c;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $borrower_former_city_c;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $borrower_former_state_c;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $borrower_former_zip_c;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $borrower_former_address_own_c = 0;

    /**
     * @ORM\Column(type="smallint", optins={"default": 0)
     */
    private $borrower_former_address_rent_c = 0;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $borrower_former_addr_no_yrs_c;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $coborr_former_street_addr_c;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $coborr_former_city_c;

    





    public function getIdC(): ?int
    {
        return $this->id;
    }

    public function setIdC(string $id_c): self
    {
        $this->id_c = $id_c;

        return $this;
    }

    public function getLookingIntoC(): ?string
    {
        return $this->looking_into_c;
    }

    public function setLookingIntoC(string $looking_into_c): self
    {
        $this->looking_into_c = $looking_into_c;

        return $this;
    }

    public function getLeadTypeC(): ?string
    {
        return $this->lead_type_c;
    }

    public function setLeadTypeC(?string $lead_type_c): self
    {
        $this->lead_type_c = $lead_type_c;

        return $this;
    }

    public function getMsLatC()
    {
        return $this->ms_lat_c;
    }

    public function setMsLatC($ms_lat_c): self
    {
        $this->ms_lat_c = $ms_lat_c;

        return $this;
    }

    public function getMsLngC()
    {
        return $this->ms_lng_c;
    }

    public function setMsLngC($ms_lng_c): self
    {
        $this->ms_lng_c = $ms_lng_c;

        return $this;
    }

    public function getMsLastGeocodedC(): ?\DateTimeInterface
    {
        return $this->ms_last_geocoded_c;
    }

    public function setMsLastGeocodedC(?\DateTimeInterface $ms_last_geocoded_c): self
    {
        $this->ms_last_geocoded_c = $ms_last_geocoded_c;

        return $this;
    }

    public function getGeocodeStateC(): ?string
    {
        return $this->geocode_state_c;
    }

    public function setGeocodeStateC(?string $geocode_state_c): self
    {
        $this->geocode_state_c = $geocode_state_c;

        return $this;
    }

    public function getOriginatorEmailC(): ?string
    {
        return $this->originator_email_c;
    }

    public function setOriginatorEmailC(?string $originator_email_c): self
    {
        $this->originator_email_c = $originator_email_c;

        return $this;
    }

    public function getOriginatorNameC(): ?string
    {
        return $this->originator_name_c;
    }

    public function setOriginatorNameC(?string $originator_name_c): self
    {
        $this->originator_name_c = $originator_name_c;

        return $this;
    }

    public function getClaimedC(): ?int
    {
        return $this->claimed_c;
    }

    public function setClaimedC(int $claimed_c): self
    {
        $this->claimed_c = $claimed_c;

        return $this;
    }

    public function getLeadOutcomeC(): ?string
    {
        return $this->lead_outcome_c;
    }

    public function setLeadOutcomeC(?string $lead_outcome_c): self
    {
        $this->lead_outcome_c = $lead_outcome_c;

        return $this;
    }

    public function getCheckBackLaterDateC(): ?\DateTimeInterface
    {
        return $this->check_back_later_date_c;
    }

    public function setCheckBackLaterDateC(?\DateTimeInterface $check_back_later_date_c): self
    {
        $this->check_back_later_date_c = $check_back_later_date_c;

        return $this;
    }

    public function getDidNotConvertNotesC(): ?string
    {
        return $this->did_not_convert_notes_c;
    }

    public function setDidNotConvertNotesC(?string $did_not_convert_notes_c): self
    {
        $this->did_not_convert_notes_c = $did_not_convert_notes_c;

        return $this;
    }

    public function getLoanNumberC(): ?string
    {
        return $this->loan_number_c;
    }

    public function setLoanNumberC(?string $loan_number_c): self
    {
        $this->loan_number_c = $loan_number_c;

        return $this;
    }

    public function getLeadExpirationC(): ?\DateTimeInterface
    {
        return $this->lead_expiration_c;
    }

    public function setLeadExpirationC(?\DateTimeInterface $lead_expiration_c): self
    {
        $this->lead_expiration_c = $lead_expiration_c;

        return $this;
    }

    public function getRefinancingTriggerC(): ?string
    {
        return $this->refinancing_trigger_c;
    }

    public function setRefinancingTriggerC(?string $refinancing_trigger_c): self
    {
        $this->refinancing_trigger_c = $refinancing_trigger_c;

        return $this;
    }

    public function getDidNotConvertReasonC(): ?string
    {
        return $this->did_not_convert_reason_c;
    }

    public function setDidNotConvertReasonC(string $did_not_convert_reason_c): self
    {
        $this->did_not_convert_reason_c = $did_not_convert_reason_c;

        return $this;
    }

    public function getDateClosedC(): ?\DateTimeInterface
    {
        return $this->date_closed_c;
    }

    public function setDateClosedC(?\DateTimeInterface $date_closed_c): self
    {
        $this->date_closed_c = $date_closed_c;

        return $this;
    }

    public function getReAssignedC(): ?int
    {
        return $this->re_assigned_c;
    }

    public function setReAssignedC(int $re_assigned_c): self
    {
        $this->re_assigned_c = $re_assigned_c;

        return $this;
    }

    public function getInbox25ScoreC(): ?int
    {
        return $this->inbox25_score_c;
    }

    public function setInbox25ScoreC(?int $inbox25_score_c): self
    {
        $this->inbox25_score_c = $inbox25_score_c;

        return $this;
    }

    public function getNmlsC(): ?string
    {
        return $this->nmls_c;
    }

    public function setNmlsC(?string $nmls_c): self
    {
        $this->nmls_c = $nmls_c;

        return $this;
    }

    public function getOptInC(): ?int
    {
        return $this->opt_in_c;
    }

    public function setOptInC(int $opt_in_c): self
    {
        $this->opt_in_c = $opt_in_c;

        return $this;
    }

    public function getOriginatorIdC(): ?string
    {
        return $this->originator_id_c;
    }

    public function setOriginatorIdC(?string $originator_id_c): self
    {
        $this->originator_id_c = $originator_id_c;

        return $this;
    }

    public function getPreviousAgentsC(): ?string
    {
        return $this->previous_agents_c;
    }

    public function setPreviousAgentsC(?string $previous_agents_c): self
    {
        $this->previous_agents_c = $previous_agents_c;

        return $this;
    }

    public function getNotifyAgentC(): ?int
    {
        return $this->notify_agent_c;
    }

    public function setNotifyAgentC(int $notify_agent_c): self
    {
        $this->notify_agent_c = $notify_agent_c;

        return $this;
    }

    public function getMarketingCampaignC(): ?string
    {
        return $this->marketing_campaign_c;
    }

    public function setMarketingCampaignC(?string $marketing_campaign_c): self
    {
        $this->marketing_campaign_c = $marketing_campaign_c;

        return $this;
    }

    public function getMarketingSourceC(): ?string
    {
        return $this->marketing_source_c;
    }

    public function setMarketingSourceC(?string $marketing_source_c): self
    {
        $this->marketing_source_c = $marketing_source_c;

        return $this;
    }

    public function getMarketingMediumC(): ?string
    {
        return $this->marketing_medium_c;
    }

    public function setMarketingMediumC(?string $marketing_medium_c): self
    {
        $this->marketing_medium_c = $marketing_medium_c;

        return $this;
    }

    public function getMarketingContentC(): ?string
    {
        return $this->marketing_content_c;
    }

    public function setMarketingContentC(?string $marketing_content_c): self
    {
        $this->marketing_content_c = $marketing_content_c;

        return $this;
    }

    public function getCorporateLeadC(): ?int
    {
        return $this->corporate_lead_c;
    }

    public function setCorporateLeadC(int $corporate_lead_c): self
    {
        $this->corporate_lead_c = $corporate_lead_c;

        return $this;
    }

    public function getRefinanceLoanIdC(): ?string
    {
        return $this->refinance_loan_id_c;
    }

    public function setRefinanceLoanIdC(?string $refinance_loan_id_c): self
    {
        $this->refinance_loan_id_c = $refinance_loan_id_c;

        return $this;
    }

    public function getPreviousOriginatorNmlsC(): ?string
    {
        return $this->previous_originator_nmls_c;
    }

    public function setPreviousOriginatorNmlsC(?string $previous_originator_nmls_c): self
    {
        $this->previous_originator_nmls_c = $previous_originator_nmls_c;

        return $this;
    }

    public function getManualOriginatorC(): ?int
    {
        return $this->manual_originator_c;
    }

    public function setManualOriginatorC(int $manual_originator_c): self
    {
        $this->manual_originator_c = $manual_originator_c;

        return $this;
    }

    public function getPreviousAgentsHistoryC(): ?string
    {
        return $this->previous_agents_history_c;
    }

    public function setPreviousAgentsHistoryC(?string $previous_agents_history_c): self
    {
        $this->previous_agents_history_c = $previous_agents_history_c;

        return $this;
    }

    public function getCrmsug4OriginatorNameC(): ?string
    {
        return $this->crmsug_4_originator_name_c;
    }

    public function setCrmsug4OriginatorNameC(?string $crmsug_4_originator_name_c): self
    {
        $this->crmsug_4_originator_name_c = $crmsug_4_originator_name_c;

        return $this;
    }

    public function getCrmsug4FromAgentNameC(): ?string
    {
        return $this->crmsug_4_from_agent_name_c;
    }

    public function setCrmsug4FromAgentNameC(?string $crmsug_4_from_agent_name_c): self
    {
        $this->crmsug_4_from_agent_name_c = $crmsug_4_from_agent_name_c;

        return $this;
    }

    public function getCrmsug4OriginatorNmlsIdC(): ?string
    {
        return $this->crmsug_4_originator_nmls_id_c;
    }

    public function setCrmsug4OriginatorNmlsIdC(?string $crmsug_4_originator_nmls_id_c): self
    {
        $this->crmsug_4_originator_nmls_id_c = $crmsug_4_originator_nmls_id_c;

        return $this;
    }

    public function getCrmsug4OriginatorAddressC(): ?string
    {
        return $this->crmsug_4_originator_address_c;
    }

    public function setCrmsug4OriginatorAddressC(?string $crmsug_4_originator_address_c): self
    {
        $this->crmsug_4_originator_address_c = $crmsug_4_originator_address_c;

        return $this;
    }

    public function getSugarcrm4OriginatorPhoneC(): ?string
    {
        return $this->sugarcrm_4_originator_phone_c;
    }

    public function setSugarcrm4OriginatorPhoneC(?string $sugarcrm_4_originator_phone_c): self
    {
        $this->sugarcrm_4_originator_phone_c = $sugarcrm_4_originator_phone_c;

        return $this;
    }

    public function getCrmsug4OriginatorTitleC(): ?string
    {
        return $this->crmsug_4_originator_title_c;
    }

    public function setCrmsug4OriginatorTitleC(?string $crmsug_4_originator_title_c): self
    {
        $this->crmsug_4_originator_title_c = $crmsug_4_originator_title_c;

        return $this;
    }

    public function getCallOrNoteC(): ?int
    {
        return $this->call_or_note_c;
    }

    public function setCallOrNoteC(int $call_or_note_c): self
    {
        $this->call_or_note_c = $call_or_note_c;

        return $this;
    }

    public function getBorrowerCurrentlyRentC(): ?int
    {
        return $this->borrower_currently_rent_c;
    }

    public function setBorrowerCurrentlyRentC(int $borrower_currently_rent_c): self
    {
        $this->borrower_currently_rent_c = $borrower_currently_rent_c;

        return $this;
    }

    public function getBorrowerCurrentlyOwnC(): ?int
    {
        return $this->borrower_currently_own_c;
    }

    public function setBorrowerCurrentlyOwnC(int $borrower_currently_own_c): self
    {
        $this->borrower_currently_own_c = $borrower_currently_own_c;

        return $this;
    }

    public function getBorrowerAddressNoYrsC(): ?string
    {
        return $this->borrower_address_no_yrs_c;
    }

    public function setBorrowerAddressNoYrsC(string $borrower_address_no_yrs_c): self
    {
        $this->borrower_address_no_yrs_c = $borrower_address_no_yrs_c;

        return $this;
    }

    public function getBorrowerFormerStreetAddreC(): ?string
    {
        $this->borrower_former_street_addre_c;
    }

    public function setBorrowerFormerStreetAddreC(string $borrower_former_street_addre_c): self
    {
        $this->borrower_former_street_addre_c = $borrower_former_street_addre_c;
        return $this;
    }

    public function getBorrowerFormerCityC(): ?string
    {
        return $this->borrower_former_city_c;
    }

    public function setBorrowerFormerCityC(string $borrower_former_city_c): self
    {
        $this->borrower_former_city_c = $borrower_former_city_c;

        return $this;
    }

    public function getBorrowerFormerStateC(): ?string
    {
        return $this->borrower_former_state_c;
    }

    public function setBorrowerFormerStateC(?string $borrower_former_state_c): self
    {
        $this->borrower_former_state_c = $borrower_former_state_c;

        return $this;
    }

    public function getBorrowerFormerZipC(): ?string
    {
        return $this->borrower_former_zip_c;
    }

    public function setBorrowerFormerZipC(?string $borrower_former_zip_c): self
    {
        $this->borrower_former_zip_c = $borrower_former_zip_c;

        return $this;
    }

    public function getBorrowerFormerAddressOwnC(): ?int
    {
        return $this->borrower_former_address_own_c;
    }

    public function setBorrowerFormerAddressOwnC(int $borrower_former_address_own_c): self
    {
        $this->borrower_former_address_own_c = $borrower_former_address_own_c;

        return $this;
    }

    public function getBorrowerFormerAddressRentC(): ?int
    {
        return $this->borrower_former_address_rent_c;
    }

    public function setBorrowerFormerAddressRentC(int $borrower_former_address_rent_c): self
    {
        $this->borrower_former_address_rent_c = $borrower_former_address_rent_c;

        return $this;
    }

    public function getBorrowerFormerAddrNoYrsC(): ?string
    {
        return $this->borrower_former_addr_no_yrs_c;
    }

    public function setBorrowerFormerAddrNoYrsC(string $borrower_former_addr_no_yrs_c): self
    {
        $this->borrower_former_addr_no_yrs_c = $borrower_former_addr_no_yrs_c;

        return $this;
    }

    public function getCoborrFormerStreetAddrC(): ?string
    {
        return $this->coborr_former_street_addr_c;
    }

    public function setCoborrFormerStreetAddrC(?string $coborr_former_street_addr_c): self
    {
        $this->coborr_former_street_addr_c = $coborr_former_street_addr_c;

        return $this;
    }

    public function getCoborrFormerCityC(): ?string
    {
        return $this->coborr_former_city_c;
    }

    public function setCoborrFormerCityC(?string $coborr_former_city_c): self
    {
        $this->coborr_former_city_c = $coborr_former_city_c;

        return $this;
    }



   


}
// end of class
// end of file