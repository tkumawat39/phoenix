<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LeadsRepository")
 */
class Leads
{
    use EntityIdTrait;

    /**
     * @Assert\NotBlank(message="Date Entered should not be blank.")
     * @Assert\DateTime(format="Y-m-d H:i:s")
     * @var string A "Y-m-d H:i:s" formatted value
     * @ORM\Column(type="datetime")
     *
     */
    private $date_entered;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank(message="Modified Date should not be blank.")
     * @Assert\DateTime(format="Y-m-d H:i:s")
     * @var string A "Y-m-d H:i:s" formatted value
     */
    private $date_modified;

    /**
     * @Assert\NotBlank(message="Modified User id should not be blank.")
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $modified_user_id;

    /**
     * @Assert\NotBlank(message="Created By should not be blank.")
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $created_by;

    /**
     *  @Assert\NotBlank(message="Description should not be blank.")
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @Assert\PositiveOrZero(message = "This value should be either positive or zero")
     * @Assert\Choice({1, 0})
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $deleted = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salutation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message="First Name should not be blank.")
     * @Assert\Length(
     *      min = 2,
     *      max = 100,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message="Last Name should not be blank.")
     * @Assert\Length(
     *      min = 2,
     *      max = 100,
     *      minMessage = "Your last name must be at least {{ limit }} characters long",
     *      maxMessage = "Your last name cannot be longer than {{ limit }} characters"
     * )
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "Title cannot be longer than {{ limit }} characters"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $twitter;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $googleplus;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $department;

    /**
     * @Assert\Choice({1, 0})
     * @ORM\Column(type="smallint")
     */
    private $do_not_call;

    /**
     * @Assert\Length(
     *      max = 12,
     *      maxMessage = "Phone Home cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone_home;

    /**
     * @Assert\Length(
     *      max = 12,
     *      maxMessage = "Mobile cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone_mobile;

    /**
     * @Assert\Length(
     *      max = 12,
     *      maxMessage = "Phone Work cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone_work;

    /**
     * @Assert\Length(
     *      max = 12,
     *      maxMessage = "Phone Other cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone_other;

    /**
     * @Assert\Length(
     *      max = 15,
     *      maxMessage = "Phone fax cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone_fax;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $primary_address_street;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $primary_address_city;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $primary_address_state;

    /**
     * @Assert\Positive
     * @Assert\Length(
     *      max = 5,
     *      maxMessage = "Primary Address Postalcode cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $primary_address_postalcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $primary_address_country;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $alt_address_street;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $alt_address_city;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $alt_address_state;

    /**
     * @Assert\Positive
     * @Assert\Length(
     *      max = 5,
     *      maxMessage = "Alternate address Postalcode cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=20)
     */
    private $alt_address_postalcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alt_address_country;

    /**
     * @ORM\Column(type="string", length=75, nullable=true)
     */
    private $assistant;

    /**
     * @Assert\Length(
     *      max = 12,
     *      maxMessage = "Phone Other cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $assistant_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @Assert\Choice({1, 0})
     * @ORM\Column(type="smallint")
     */
    private $converted;

    /**
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "Refered by cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $refered_by;

    /**
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "Lead source cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $lead_source;

    /**
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Lead description cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lead_source_description;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $status;

    /**
     * @ORM\Column(type="text")
     */
    private $status_description;

    /**
     * @ORM\Column(type="string", length=36)
     */
    private $reports_to_id;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $dnb_principal_id;

    /**
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Account name cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $account_name;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $contact_id;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $account_id;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $opportunity_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $opportunity_name;

    /**
     * @Assert\Positive
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $opportunity_amount;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $campaign_id;

    /**
     * @Assert\Date
     * @var string A "Y-m-d" formatted value
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $portal_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $portal_app;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $preferred_language;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $mkto_sync;

    /**
     * @Assert\Choice({1, 0})
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mkto_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mkto_lead_score;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $assigned_user_id;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $team_id;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $team_set_id;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $acl_team_set_id;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $dri_workflow_template_id;



    public function getDateEntered(): ?\DateTimeInterface
    {
        return $this->date_entered;
    }

    public function setDateEntered(?\DateTimeInterface $date_entered): self
    {
        $this->date_entered = $date_entered;

        return $this;
    }

    public function getDateModified(): ?\DateTimeInterface
    {
        return $this->date_modified;
    }

    public function setDateModified(?\DateTimeInterface $date_modified): self
    {
        $this->date_modified = $date_modified;

        return $this;
    }

    public function getModifiedUserId(): ?string
    {
        return $this->modified_user_id;
    }

    public function setModifiedUserId(?string $modified_user_id): self
    {
        $this->modified_user_id = $modified_user_id;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->created_by;
    }

    public function setCreatedBy(?string $created_by): self
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDeleted(): ?int
    {
        return $this->deleted;
    }

    public function setDeleted(int $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getSalutation(): ?string
    {
        return $this->salutation;
    }

    public function setSalutation(?string $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(?string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getGoogleplus(): ?string
    {
        return $this->googleplus;
    }

    public function setGoogleplus(?string $googleplus): self
    {
        $this->googleplus = $googleplus;

        return $this;
    }

    public function getDepartment(): ?string
    {
        return $this->department;
    }

    public function setDepartment(?string $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getDoNotCall(): ?int
    {
        return $this->do_not_call;
    }

    public function setDoNotCall(int $do_not_call): self
    {
        $this->do_not_call = $do_not_call;

        return $this;
    }

    public function getPhoneHome(): ?string
    {
        return $this->phone_home;
    }

    public function setPhoneHome(?string $phone_home): self
    {
        $this->phone_home = $phone_home;

        return $this;
    }

    public function getPhoneMobile(): ?string
    {
        return $this->phone_mobile;
    }

    public function setPhoneMobile(?string $phone_mobile): self
    {
        $this->phone_mobile = $phone_mobile;

        return $this;
    }

    public function getPhoneWork(): ?string
    {
        return $this->phone_work;
    }

    public function setPhoneWork(?string $phone_work): self
    {
        $this->phone_work = $phone_work;

        return $this;
    }

    public function getPhoneOther(): ?string
    {
        return $this->phone_other;
    }

    public function setPhoneOther(?string $phone_other): self
    {
        $this->phone_other = $phone_other;

        return $this;
    }

    public function getPhoneFax(): ?string
    {
        return $this->phone_fax;
    }

    public function setPhoneFax(?string $phone_fax): self
    {
        $this->phone_fax = $phone_fax;

        return $this;
    }

    public function getPrimaryAddressStreet(): ?string
    {
        return $this->primary_address_street;
    }

    public function setPrimaryAddressStreet(?string $primary_address_street): self
    {
        $this->primary_address_street = $primary_address_street;

        return $this;
    }

    public function getPrimaryAddressCity(): ?string
    {
        return $this->primary_address_city;
    }

    public function setPrimaryAddressCity(?string $primary_address_city): self
    {
        $this->primary_address_city = $primary_address_city;

        return $this;
    }

    public function getPrimaryAddressState(): ?string
    {
        return $this->primary_address_state;
    }

    public function setPrimaryAddressState(?string $primary_address_state): self
    {
        $this->primary_address_state = $primary_address_state;

        return $this;
    }

    public function getPrimaryAddressPostalcode(): ?string
    {
        return $this->primary_address_postalcode;
    }

    public function setPrimaryAddressPostalcode(?string $primary_address_postalcode): self
    {
        $this->primary_address_postalcode = $primary_address_postalcode;

        return $this;
    }

    public function getPrimaryAddressCountry(): ?string
    {
        return $this->primary_address_country;
    }

    public function setPrimaryAddressCountry(?string $primary_address_country): self
    {
        $this->primary_address_country = $primary_address_country;

        return $this;
    }

    public function getAltAddressStreet(): ?string
    {
        return $this->alt_address_street;
    }

    public function setAltAddressStreet(?string $alt_address_street): self
    {
        $this->alt_address_street = $alt_address_street;

        return $this;
    }

    public function getAltAddressCity(): ?string
    {
        return $this->alt_address_city;
    }

    public function setAltAddressCity(?string $alt_address_city): self
    {
        $this->alt_address_city = $alt_address_city;

        return $this;
    }

    public function getAltAddressState(): ?string
    {
        return $this->alt_address_state;
    }

    public function setAltAddressState(?string $alt_address_state): self
    {
        $this->alt_address_state = $alt_address_state;

        return $this;
    }

    public function getAltAddressPostalcode(): ?string
    {
        return $this->alt_address_postalcode;
    }

    public function setAltAddressPostalcode(string $alt_address_postalcode): self
    {
        $this->alt_address_postalcode = $alt_address_postalcode;

        return $this;
    }

    public function getAltAddressCountry(): ?string
    {
        return $this->alt_address_country;
    }

    public function setAltAddressCountry(?string $alt_address_country): self
    {
        $this->alt_address_country = $alt_address_country;

        return $this;
    }

    public function getAssistant(): ?string
    {
        return $this->assistant;
    }

    public function setAssistant(?string $assistant): self
    {
        $this->assistant = $assistant;

        return $this;
    }

    public function getAssistantPhone(): ?string
    {
        return $this->assistant_phone;
    }

    public function setAssistantPhone(?string $assistant_phone): self
    {
        $this->assistant_phone = $assistant_phone;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getConverted(): ?int
    {
        return $this->converted;
    }

    public function setConverted(int $converted): self
    {
        $this->converted = $converted;

        return $this;
    }

    public function getReferedBy(): ?string
    {
        return $this->refered_by;
    }

    public function setReferedBy(?string $refered_by): self
    {
        $this->refered_by = $refered_by;

        return $this;
    }

    public function getLeadSource(): ?string
    {
        return $this->lead_source;
    }

    public function setLeadSource(?string $lead_source): self
    {
        $this->lead_source = $lead_source;

        return $this;
    }

    public function getLeadSourceDescription(): ?string
    {
        return $this->lead_source_description;
    }

    public function setLeadSourceDescription(?string $lead_source_description): self
    {
        $this->lead_source_description = $lead_source_description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusDescription(): ?string
    {
        return $this->status_description;
    }

    public function setStatusDescription(string $status_description): self
    {
        $this->status_description = $status_description;

        return $this;
    }

    public function getReportsToId(): ?string
    {
        return $this->reports_to_id;
    }

    public function setReportsToId(string $reports_to_id): self
    {
        $this->reports_to_id = $reports_to_id;

        return $this;
    }

    public function getDnbPrincipalId(): ?string
    {
        return $this->dnb_principal_id;
    }

    public function setDnbPrincipalId(?string $dnb_principal_id): self
    {
        $this->dnb_principal_id = $dnb_principal_id;

        return $this;
    }

    public function getAccountName(): ?string
    {
        return $this->account_name;
    }

    public function setAccountName(?string $account_name): self
    {
        $this->account_name = $account_name;

        return $this;
    }

    public function getContactId(): ?string
    {
        return $this->contact_id;
    }

    public function setContactId(?string $contact_id): self
    {
        $this->contact_id = $contact_id;

        return $this;
    }

    public function getAccountId(): ?string
    {
        return $this->account_id;
    }

    public function setAccountId(?string $account_id): self
    {
        $this->account_id = $account_id;

        return $this;
    }

    public function getOpportunityId(): ?string
    {
        return $this->opportunity_id;
    }

    public function setOpportunityId(?string $opportunity_id): self
    {
        $this->opportunity_id = $opportunity_id;

        return $this;
    }

    public function getOpportunityName(): ?string
    {
        return $this->opportunity_name;
    }

    public function setOpportunityName(?string $opportunity_name): self
    {
        $this->opportunity_name = $opportunity_name;

        return $this;
    }

    public function getOpportunityAmount(): ?string
    {
        return $this->opportunity_amount;
    }

    public function setOpportunityAmount(?string $opportunity_amount): self
    {
        $this->opportunity_amount = $opportunity_amount;

        return $this;
    }

    public function getCampaignId(): ?string
    {
        return $this->campaign_id;
    }

    public function setCampaignId(?string $campaign_id): self
    {
        $this->campaign_id = $campaign_id;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getPortalName(): ?string
    {
        return $this->portal_name;
    }

    public function setPortalName(?string $portal_name): self
    {
        $this->portal_name = $portal_name;

        return $this;
    }

    public function getPortalApp(): ?string
    {
        return $this->portal_app;
    }

    public function setPortalApp(?string $portal_app): self
    {
        $this->portal_app = $portal_app;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getPreferredLanguage(): ?string
    {
        return $this->preferred_language;
    }

    public function setPreferredLanguage(?string $preferred_language): self
    {
        $this->preferred_language = $preferred_language;

        return $this;
    }

    public function getMktoSync(): ?int
    {
        return $this->mkto_sync;
    }

    public function setMktoSync(?int $mkto_sync): self
    {
        $this->mkto_sync = $mkto_sync;

        return $this;
    }

    public function getMktoId(): ?int
    {
        return $this->mkto_id;
    }

    public function setMktoId(?int $mkto_id): self
    {
        $this->mkto_id = $mkto_id;

        return $this;
    }

    public function getMktoLeadScore(): ?int
    {
        return $this->mkto_lead_score;
    }

    public function setMktoLeadScore(?int $mkto_lead_score): self
    {
        $this->mkto_lead_score = $mkto_lead_score;

        return $this;
    }

    public function getAssignedUserId(): ?string
    {
        return $this->assigned_user_id;
    }

    public function setAssignedUserId(?string $assigned_user_id): self
    {
        $this->assigned_user_id = $assigned_user_id;

        return $this;
    }

    public function getTeamId(): ?string
    {
        return $this->team_id;
    }

    public function setTeamId(?string $team_id): self
    {
        $this->team_id = $team_id;

        return $this;
    }

    public function getTeamSetId(): ?string
    {
        return $this->team_set_id;
    }

    public function setTeamSetId(?string $team_set_id): self
    {
        $this->team_set_id = $team_set_id;

        return $this;
    }

    public function getAclTeamSetId(): ?string
    {
        return $this->acl_team_set_id;
    }

    public function setAclTeamSetId(?string $acl_team_set_id): self
    {
        $this->acl_team_set_id = $acl_team_set_id;

        return $this;
    }

    public function getDriWorkflowTemplateId(): ?string
    {
        return $this->dri_workflow_template_id;
    }

    public function setDriWorkflowTemplateId(?string $dri_workflow_template_id): self
    {
        $this->dri_workflow_template_id = $dri_workflow_template_id;

        return $this;
    }
}
// end of class
// end of file
