<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;
     /**
     * @ORM\Column(type="json")
     */
    private $roles = [];
     /**
     * @var string
     *
     * @ORM\Column(name="first_name",type="string")
     * @Groups("api")
     */
    private $first_name;
    /**
     * @var string
     *
     * @ORM\Column(name="last_name",type="string")
     * @Groups("api")
     */
    private $last_name;
    /**
     * @ORM\Column(name="contact",type="string")
     * @Groups("api")
     */
    private $contact;
    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups("api")
     */
    private $email;
    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups("api")
     */
    private $password;
	/**
     * @ORM\Column(name="enabled", type="boolean")
     * @Groups("api")
     */
    private $enabled;
     /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;
     /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;
    public function __construct()
    {
        $this->status = true;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
    public function setFirstName(string $first_name): void
    {
        $this->first_name = $first_name;
    }
    public function setLastName(string $last_name): void
    {
        $this->last_name = $last_name;
    }
    public function getFirstName(): ?string
    {
        return $this->first_name;
    }
    public function getLastName(): ?string
    {
        return $this->last_name;
    }
    public function setContact(string $contact): void
    {
        $this->contact = $contact;
    }
    public function getContact(): ?string
    {
        return $this->contact;
    }
     /**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }
    /**
     * Get enabled.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }
    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }
    /**
     * @param \DateTime|null $time
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $time = null): UserInterface
    {
        $this->createdAt = $time;
        return $this;
    }
    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }
    /**
     * @param \DateTime|null $time
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTime $time = null): UserInterface
    {
        $this->updatedAt = $time;
        return $this;
    }
} 
