/* Developer by Mohit Chawla */
$(document).ready(function() {
    var quotes = [{
        "quote": "<span class='quote-content'>The only limit to our realization of tommorow will be our doubts of today.</span><br />Franklin D. Roosevelt"
    }, {
        "quote": "<span class='quote-content'>If you really look closely, most overnight successes look a long time.</span><br />Steve Jobs"       
    }, {
        "quote": "<span class='quote-content'>The ones who are crazy enough to think they can change the world, are the ones that do.</span><br />Anonymous"
    }, {
        "quote": "<span class='quote-content'>If you are not willng to risk the usual, you will have to settle for the ordinary.</span><br />Jim Rohn"
    }, {
        "quote": "<span class='quote-content'>Success is not final; failure is not fatal: It is the courage to continue that counts.</span><br/>Winston Churchill"
    }, {
        "quote": "<span class='quote-content'>Successful people keep moving. They make mistakes, but they don't quit.</span><br />Conrad Hilton"
    }, {
        "quote": "<span class='quote-content'>The successful warrior is the average man, with laser-like focus.</span><br />Bruce Lee"
    }, {
        "quote": "<span class='quote-content'>Fall seven times and stand up eight.</span><br />Japanese Proverb"
    }, {
        "quote": "<span class='quote-content'>The only place where success comes before work is in the dictionary.</span> <br />Vidal Sassoon"
    }, {
        "quote": "<span class='quote-content'>Patience is a virtue, and Im learning patience. It's a tough  lesson.</span><br />Elon Musk"
    }, {
        "quote": "<span class='quote-content'>Perpetual optimism is a force multiplier.</span> <br />Colin Powell"
    }, {
        "quote": "<span class='quote-content'>You cannot be lost on a road that is straight.</span> <br />Proverb"
    }];

    function getQuote() {
        var randomIndex = Math.floor(Math.random() * quotes.length);
        var selectedQuote = quotes[randomIndex];
        $('#main-quote').html(selectedQuote.quote);
    }
    getQuote();
});


	
	
