// JavaScript Document


// $.validator.setDefaults({
// 		submitHandler: function() {
// 		}
// 	});
$(document).ready(function () {

	jQuery("#pinfill").on('change', function () {

		var numArray = [1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888, 9999, 0000, 0987, 9876, 8765, 7654, 6543, 5432, 4321, 1234, 2345, 3456, 4567, 5678, 6789, 7890, 2468, 1357];

		var pinfill = $("#pinfill").val();
		if (jQuery.inArray(parseInt(pinfill), numArray) > -1) {
			$('#pin-error').html("too easy!");
		} else {
			$('#pin-error').html('');
		}
	});
	
	$('select.custom').each(function () {
		var $this = $(this), numberOfOptions = $(this).children('option').length;
		$(this).addClass('select-hidden');
		$(this).wrap('<div class="select"></div>');
		$(this).after('<div class="select-styled dropdown-small"></div>');

		var $styledSelect = $this.next('div.select-styled');
		$styledSelect.text($this.children('option').eq(0).text());
		$styledSelect.text($this.children('.start-time option').eq(6).html());
		$styledSelect.text($this.children('.end-time option').eq(17).html());


		var $list = $('<ul />', {
			'class': 'select-options sort-dropdown'
		}).insertAfter($styledSelect);

		for (var i = 0; i < numberOfOptions; i++) {
			$('<li />', {
				text: $this.children('option').eq(i).text(),
				value: $this.children('option').eq(i).val(),
				selected: $this.children('selected').eq(i).val()
			}).appendTo($list).wrapInner("<span></span>");
		}

		var $listItems = $list.children('li');
		$('.select-hidden option[value="no"]').attr("selected", "selected");
		$styledSelect.click(function (e) {
			e.stopPropagation();
			$('div.select-styled.active').not(this).each(function () {
				$(this).removeClass('active').next('ul.select-options').hide();
			});
			$(this).toggleClass('active').next('ul.select-options').toggle();
		});

		$listItems.click(function (e) {
			e.stopPropagation();
			$styledSelect.text($(this).text()).removeClass('active');
			$this.val($(this).attr('value'));
			$this.val($(this).attr('selected'));
			$list.hide();
			//console.log($this.val());
		});

		$(document).click(function () {
			$styledSelect.removeClass('active');
			$list.hide();
		});

	});



});

// $().ready(function () {

// 	jQuery.validator.addMethod("zipcode", function (value, element) {
// 		return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
// 	}, "Please provide a valid zipcode.");

// 	$("#createLead").validate({
// 		// Specify validation rules
// 		rules: {
// 			// The key name on the left side is the name attribute
// 			// of an input field. Validation rules are defined
// 			// on the right side
// 			phone: "required",
// 			ofphone: "required",
// 			city: "required",
// 			zipcode: true,
// 			email: {
// 				required: true,
// 				// Specify that email should be validated
// 				// by the built-in "email" rule
// 				email: true
// 			},
// 			zipcode: {
// 				required: true,
// 				minlength: 4,
// 				maxlength: 5
// 			}
// 		},
// 		// Specify validation error messages
// 		messages: {
// 			phone: "Please enter your mobile number",
// 			ofphone: "Please enter your office phone number",
// 			city: "Please enter city",
// 			zipcode: {
// 				required: "Please provide a zip code",
// 				minlength: "Your zip code must be at least 4 characters long"
// 			},
// 			email: "Please enter a valid email address"
// 		},
// 		// Make sure the form is submitted to the destination defined
// 		// in the "action" attribute of the form when valid
// 		submitHandler: function (form) {
// 			form.submit();
// 		}
// 	});


// 	$("#pingenerate").validate({
// 		rules: {
// 			pin: "required",
// 			lastname: "required",
// 			username: {
// 				required: true,
// 				minlength: 4
// 			},
// 			pin: {
// 				required: true,
// 				minlength: 4,
// 			},
// 			confirm_pin: {
// 				required: true,
// 				minlength: 4,
// 				equalTo: "#pinfill"
// 			},

// 			agree: "required"
// 		},
// 		messages: {
// 			pin: "Please enter your firstname",
// 			pin: {
// 				required: "Please provide a pin",
// 				minlength: "too easy!"
// 			},
// 			confirm_pin: {
// 				required: "Please provide a password",
// 				minlength: "Your pin must be at least 4 characters long",
// 				equalTo: "numbers do not match"
// 			},
// 		}
// 	});

// 	$("#resetpassword").validate({
// 		rules: {
// 			newpassword: {
// 				required: true,
// 				minlength: 8,
// 				pwcheck: true
// 			},
// 			confirm_password: {
// 				required: true,
// 				minlength: 8,
// 				equalTo: "#newpassword"
// 			},
// 			agree: "required"
// 		},
// 		messages: {
// 			newpassword: {
// 				required: "required",
// 				minlength: "must be at least 8 characters and include one special character !@#$",
// 				pwcheck: "special character !@#$"
// 			},
// 			confirm_password: {
// 				required: "confirm password",
// 				minlength: false,
// 				equalTo: "passwords do not match"
// 			},

// 		}

// 	});
// 	$.validator.addMethod("pwcheck",
// 		function (value, element) {
// 			return /^[a-zA-Z0-9.!#$%&\d=!\-@._*]+$/.test(value);
// 		});
// });

$(document).ready(function (e) {

	var dots = "...";
	$(".textWrap").text(function (index, currentText) {
		return currentText.substr(0, 30) + dots;
	});
	$(".textWrapLong").text(function (index, currentText) {
		return currentText.substr(0, 50) + dots;
	});
});
$('.txtbox').keypress(function () {
	var txtWidth = $(this).width();
	var cs = $(this).val().length;

	if (cs > 20) {
		$(this).width(txtWidth + 5);
	}
});
/*$(document).ready(function() {
    $('#leads').DataTable();
	
});

$(document).ready(function() {
    $('.contacted').DataTable();
});*/

$(function (e) {
	$("#sortable").sortable({
		cancel: ".item-disabled",
		items: "li:not(.item-disabled)"
	});
	$("#sortable li.active").append('<span class="remove-list">x</span>');
	$("#sortable .remove-list").click(function () {
		$(this).parent().addClass('hide');
	});
	$(".revert").click(function () {
		$("#sortable li").removeClass('hide');
	});
});

$(function (e) {
	$("#sortable2").sortable({
		cancel: ".item-disabled",
		items: "li:not(.item-disabled)"
	});
	$("#sortable2 li.active").append('<span class="remove-list">x</span>');
	$("#sortable2 .remove-list").click(function () {
		$(this).parent().addClass('hide');
	});
	$(".revert").click(function () {
		$("#sortable2 li").removeClass('hide');
	});
});



/*$('#datepicker').datepicker({
	numberOfMonths: 2
});
*/


var headers = ["H1", "H2", "H3", "H4", "H5", "H6"];

$(".accordions").click(function (e) {
	var target = e.target,
		name = target.nodeName.toUpperCase();

	if ($.inArray(name, headers) > -1) {

		var subItem = $(target).next();

		//slideUp all elements (except target) at current depth or greater
		var depth = $(subItem).parents().length;
		var allAtDepth = $(".accordions .second-stage").filter(function () {
			if ($(this).parents().length >= depth && this !== subItem.get(0)) {
				return true;

			}

		});
		//$(allAtDepth).slideUp("fast");
		//$(target).removeClass('active-toggle');
		//$(".accordions").find(target).closest('.accordion-head').addClass('active-toggle').siblings().removeClass("active-toggle");

		//slideToggle target content and adjust bottom border if necessary
		subItem.slideToggle("fast", function () {
			//$(".accordions :visible:last").css("border-radius","0 0 10px 10px");

		});
		//$(subItem).toggleClass('hello');
		$(target).toggleClass("active-toggle");

		/*if($(target).hasClass("active-toggle")){
			$(target).removeClass("active-toggle");
			//console.log();
		}else{		
			$(target).parents('.accordions').find('h2').removeClass("active-toggle");		
			$(target).addClass('active-toggle');
		}*/


		//$(target).addClass('hello');
	}
});

$(".accordions-edit").click(function (e) {
	var target = e.target,
		name = target.nodeName.toUpperCase();

	if ($.inArray(name, headers) > -1) {

		var subItem = $(target).next();

		//slideUp all elements (except target) at current depth or greater
		var depth = $(subItem).parents().length;
		var allAtDepth = $(".accordions-edit .second-stage").filter(function () {
			if ($(this).parents().length >= depth && this !== subItem.get(0)) {
				return true;

			}

		});
		//$(allAtDepth).slideUp("fast");
		//$(target).removeClass('active-toggle');
		//$(".accordions").find(target).closest('.accordion-head').addClass('active-toggle').siblings().removeClass("active-toggle");

		//slideToggle target content and adjust bottom border if necessary
		subItem.slideToggle("fast", function () {
			$(".accordions-edit :visible:last").css("border-radius", "0 0 10px 10px");

		});

		$(target).toggleClass("active-toggle");
		/*if($(target).hasClass("active-toggle")){
			$(target).removeClass("active-toggle");
			//console.log();
		}else{		
			$(target).parents('.accordions-edit').find('h2').removeClass("active-toggle");		
			$(target).addClass('active-toggle');
		}*/


		//$(target).addClass('hello');
	}
});





jQuery('.dropdown-toggle-custom').on('click', function (e) {
	$(this).next().parent().addClass('show');
});
/*jQuery('.dropdown-menu.keep-open').on('click', function (e) {
  //e.stopPropagation();
});*/

$(document).on('click', '.dropdown-menu.custom-dropdown', function (e) {
	e.stopPropagation();
});

$('.dropdown-menu.custom-dropdown a').on('click', function (e) {
	//e.stopPropagation();
});


/*
$('.close-dropdown-menu').on("click", function(event){
	var $trigger = $(".dropdown");
	if($trigger !== event.target && !$trigger.has(event.target).length){
		$(".dropdown-menu").hide();
	}      
	
	$(this).parentDropdown("dropdown-menu");
	return false;
        
});
*/
$(".close-dropdown-menu,.close-btn").click(function () {
	$(".custom-dropdown").removeClass('show');

	//$(".dropdown-menu").next().toggle();
	// return false;
});
$(function () {
	$(".event-datepicker").datepicker({
		numberOfMonths: 2,
		dayNamesMin: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
	});
});

$(function () {
	var currentDate = new Date();
	$(".small-datepicker").datepicker({
		dayNamesMin: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],

	});
	$(".small-datepicker").datepicker("setDate", currentDate)
});
$(function () {
	var currentDate = new Date();
	$(".future-datepicker").each(function () {
		$(this).datepicker({
			dayNamesMin: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			minDate: 0,
			dateFormat: 'mm/dd/yy',
			onSelect: function (d) {
				$('.preview_date').text(d);
			}

		});
		$(".small-datepicker").datepicker("setDate", currentDate)
	});
});
$(function () {
	$(".remider-datepicker").datepicker({
		dayNamesMin: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
		minDate: 0,
		dateFormat: 'mm/dd/yy'
	});
});


/*$(function() {
$('#reminderTime').timepicker();
});	
	*/
/*$(document).ready(function(e) {
    $('.show .small-datepicker').datepicker({
	  }).on('click','span, td, th, a.ui-state-default', function(e) {
		// ensures the dropdown is hidden
		$(this).find('.show .dropdown-menu').hide();
	  });
	 
});	*/


/*$('.small-datepicker').datepicker().on('changeDate', function (ev) {
	  $(this).closest('.dropdown-menu').one('hide.bs.dropdown', function(ev) {
		 return true;
	 });
  });*/

/*$('.yourDatePicker').on('hide', function(event) {
	  event.stopPropagation();
	  $(this).closest('.yourDropDown').one('hide.bs.dropdown', function(ev) {
		 return false;
	 });
  });*/

$(document).ready(function () {
	$('.editbtn').on('click', function () {
		var currentTD = $(this).parents('.editable-element').find('.edit-item');
		if ($(this).html() == 'Edit') {
			currentTD = $(this).parents('.editable-element').find('.edit-item');
			$.each(currentTD, function () {
				$(this).prop('contenteditable', true);
				$(this).parents('.editable-element').addClass('visible');
				$(currentTD).prop('contenteditable', true).attr('id', 'contactAssociation');
			});
		} else {
			$.each(currentTD, function () {
				$(this).prop('contenteditable', false);
				$(this).parents('.editable-element').removeClass('visible');
			});
		}

		$(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')

		$('[contenteditable]').on('keydown', function (e) {
			var key = e.keyCode,
				el = $(this)[0];
			// If Enter    
			if (key === 13) {
				e.preventDefault(); // Prevent the <div /> creation.
				$.each(currentTD, function () {
					$(this).prop('contenteditable', false);
					$(this).parents('.editable-element').removeClass('visible');
				}
				);
			};

		});

		$(document).mouseup(function (e) {

			if ($(e.target).closest(".editable-element").length === 0) {
				$.each(currentTD, function () {
					$(this).prop('contenteditable', false);
					$(this).parents('.editable-element').removeClass('visible');
				})
			}

		});
	});

});


$(function () {
	$('#gettingScore').on('show.bs.modal', function () {
		var myModal = $(this);
		clearTimeout(myModal.data('hideInterval'));
		myModal.data('hideInterval', setTimeout(function () {
			myModal.modal('hide');
			$('#success').modal('show');
		}, 3000));
	});
});


jQuery(document).ready(function () {
	jQuery('.displaybtn').click(function (e) {
		e.preventDefault();
		jQuery('tr.hide').toggleClass('show');
	});
});

jQuery(document).ready(function () {
	jQuery('.displaybtn').click(function (e) {
		e.preventDefault();
		jQuery('tr.hide').toggleClass('show');
		jQuery(this).toggleClass('selected-view');

		if (jQuery('.displaybtn').hasClass('selected-view')) { //existing class
			jQuery('.displaybtn span').text('Hide');
		} else {
			jQuery('.displaybtn span').text('Show');
		}
	});

});

jQuery(document).ready(function () {
	jQuery('.showmore').click(function (e) {
		e.preventDefault();
		jQuery('.info.hide').toggleClass('moreshow');
		jQuery('.hideafter,.showmore').toggleClass('hide');
	});
});



// Javascript to enable link to tab
var hash = document.location.hash;
if (hash) {
	console.log(hash);
	$("a.tab-page[href=\\" + hash + "]profil").tab('show');
}

// Change hash for page-reload
$('a.tab-page').on('shown.bs.tab', function (e) {
	window.location.hash = e.target.hash;
});

/*Chart*/

function ctl() {

	/*Chart 1*/
	CanvasJS.addColorSet("blueShades",
		[//colorSet Array

			"#263a8e",
			"#1ea1b8",
		]);
	var chart = new CanvasJS.Chart("chartContainer1",
		{
			animationEnabled: true,
			colorSet: "blueShades",
			title: {
				//text: "Pie Chart",
			},
			data: [
				{
					type: "pie",
					showInLegend: true,
					startAngle: 300,
					dataPoints: [
						{ y: 135, legendText: "Did Not Take Apps, 135", indexLabel: "Did Not Take Apps, 135" },
						{ y: 24, legendText: "Applications, 24", indexLabel: "Applications, 24" },
					]
				},
			]
		});
	chart.render();

	/*Chart 2*/
	var chart = new CanvasJS.Chart("chartContainer2",
		{
			animationEnabled: true,
			colorSet: "blueShades",
			title: {
				// text: "Spline Area Chart"
			},
			axisX: {
				interval: 10,
			},
			data: [{
				type: "funnel",
				neckWidth: 100,
				neckHeight: 0,
				horizontalAlign: "center",
				indexLabelPlacement: "inside",
				showInLegend: true,
				indexLabelFontColor: "white",
				toolTipContent: "<b>{label}</b>: {y} <b>({percentage}%)</b>",
				indexLabel: "{label} ({percentage}%)",
				dataPoints: [
					{ y: 1400, legendText: "Processing Complete Amount:  $410,422 Count:  2", indexLabel: "Processing Complete Amount:  $410,422 Count:  2" },
					{ y: 1212, legendText: "Assigned to Underwriter Amount:  $200,000 Count:  1", indexLabel: "Assigned to Underwriter Amount:  $200,000 Count:  1" },
				]
			}]
		});
	chart.render();

	/*Chart 3*/



	var chart = new CanvasJS.Chart("chartContainer3", {
		title: {
			text: "Total Amount: $415,732 Count:3",
			wrap: true,
			maxWidth: 190,
			fontSize: 13,
			fontFamily: "Arial",
			fontWeight: "bold",
			fontColor: '#1ea1b8'
		},
		axisX: {
			valueFormatString: "MMM YYYY",
			interval: 1,
			intervalType: "month",
			labelFontColor: '#9c9c9c',
			labelFontWeight: "bold",
			tickLength: 20,
			tickColor: "#ffffff",

		},
		axisY: [{
			title: "Count",
			fontSize: 12,
			valueFormatString: "#,0#",
			lineColor: "#369EAD",
			lineThickness: 2,
			tickColor: "#369EAD",
			labelFontColor: "#9b9b9b",
			titleFontColor: "#9b9b9b",
			tickLength: 10,
			tickThickness: 2,
			gridDashType: "dash",
			gridColor: "#d1d1d1",
			//interval: 10
		}
		],
		axisY2: {
			title: "Amount",
			fontSize: 12,
			lineColor: "#263a8e",
			lineThickness: 2,
			tickColor: "#263a8e",
			labelFontColor: "#9b9b9b",
			titleFontColor: "#9b9b9b",
			tickLength: 10,
			tickThickness: 2,
			prefix: "$",
			suffix: "k",
			interval: 40,
		},
		toolTip: {
			shared: true
		},
		legend: {
			//cursor: "pointer",
			//itemclick: toggleDataSeries
		},
		data: [
			{
				type: "line",
				name: "Count",
				color: "#1ea1b8",
				markerType: "circle",
				markerColor: 'white',
				markerBorderColor: "#369EAD",
				markerBorderThickness: 1,
				//showInLegend: true,
				dataPoints: [
					{ x: new Date(2018, 00, 0), y: 0 },
					{ x: new Date(2018, 01, 14), y: 33.9 },
					{ x: new Date(2018, 02, 21), y: 26.0 },
					{ x: new Date(2018, 03, 28), y: 15.8 },
					{ x: new Date(2018, 04, 4), y: 18.6 },
					{ x: new Date(2018, 05, 11), y: 34.6 },
					{ x: new Date(2018, 06, 18), y: 37.7 },
					{ x: new Date(2018, 07, 25), y: 24.7 },
					{ x: new Date(2018, 08, 4), y: 35.9 },
					{ x: new Date(2018, 09, 11), y: 12.8 },
					{ x: new Date(2018, 10, 18), y: 38.1 },
					{ x: new Date(2018, 11, 25), y: 42.4 }
				]
			},
			{
				type: "line",
				name: "Amount",
				color: "#334695",
				axisYType: "secondary",
				markerType: "circle",
				markerColor: 'white',
				markerBorderColor: "#263a8e",
				markerBorderThickness: 1,
				//showInLegend: true,
				dataPoints: [
					{ x: new Date(2018, 00, 0), y: 0 },
					{ x: new Date(2018, 01, 14), y: 44.3 },
					{ x: new Date(2018, 02, 21), y: 28.7 },
					{ x: new Date(2018, 03, 28), y: 22.5 },
					{ x: new Date(2018, 04, 4), y: 25.6 },
					{ x: new Date(2018, 05, 11), y: 45.7 },
					{ x: new Date(2018, 06, 18), y: 54.6 },
					{ x: new Date(2018, 07, 25), y: 32.0 },
					{ x: new Date(2018, 08, 4), y: 43.9 },
					{ x: new Date(2018, 09, 11), y: 26.4 },
					{ x: new Date(2018, 10, 18), y: 40.3 },
					{ x: new Date(2018, 11, 25), y: 54.2 }
				]
			}]
	});
	chart.render();



	/*Chart 4*/
	CanvasJS.addColorSet("verticalShades",
		[//colorSet Array

			"#1ea1b8",
			"#263a8e",
		]);

	var chart = new CanvasJS.Chart("chartContainer4", {
		colorSet: "verticalShades",
		dataPointMaxWidth: 30,
		animationEnabled: true,
		title: {
			//text: "Daily High Temperature at Different Beaches"
		},
		axisX: {
			valueFormatString: "MMM YYYY",
			interval: 1,
			intervalType: "month",
			labelFontColor: '#9c9c9c',
			labelFontWeight: "bold",
			tickLength: 20,
			tickColor: "#ffffff",

		},
		axisY: {
			title: "Count",
			lineColor: "#369EAD",
			tickColor: "#369EAD",
			labelFontColor: "#9b9b9b",
			titleFontColor: "#9b9b9b",
			interval: 4,
			lineThickness: 0,
			gridDashType: "dash",
			gridColor: "#d1d1d1",
		},
		legend: {
			cursor: "pointer",
			fontSize: 11,
		},
		toolTip: {
			shared: true
		},
		data: [{
			name: "Incoming Leads",
			type: "column",
			indexLabelPlacement: "inside",
			indexLabelFontColor: 'white',
			showInLegend: true,
			dataPoints: [
				{ x: new Date(2019, 00, 24), y: 1, indexLabel: "1" },
				{ x: new Date(2019, 01, 25), y: 3, indexLabel: "3" },
				{ x: new Date(2019, 02, 26), y: 15, indexLabel: "15" },
				{ x: new Date(2019, 03, 27), y: 0 },
				{ x: new Date(2019, 04, 28), y: 0 },
				{ x: new Date(2019, 05, 29), y: 0 },
				{ x: new Date(2019, 06, 30), y: 0 },
				{ x: new Date(2019, 07, 27), y: 0 },
				{ x: new Date(2019, 08, 28), y: 0 },
				{ x: new Date(2019, 09, 29), y: 0 },
				{ x: new Date(2019, 10, 30), y: 0 },
				{ x: new Date(2019, 11, 30), y: 0 }
			]
		},
		{
			name: "Applications",
			type: "column",
			indexLabelPlacement: "inside",
			indexLabelFontColor: 'white',
			yValueFormatString: "#0.##",
			showInLegend: true,
			dataPoints: [
				{ x: new Date(2019, 00, 24), y: 0 },
				{ x: new Date(2019, 01, 25), y: 2, indexLabel: "2" },
				{ x: new Date(2019, 02, 26), y: 7, indexLabel: "7" },
				{ x: new Date(2019, 03, 27), y: 0 },
				{ x: new Date(2019, 04, 28), y: 0 },
				{ x: new Date(2019, 05, 29), y: 0 },
				{ x: new Date(2019, 06, 30), y: 0 },
				{ x: new Date(2019, 07, 27), y: 0 },
				{ x: new Date(2019, 08, 28), y: 0 },
				{ x: new Date(2019, 09, 29), y: 0 },
				{ x: new Date(2019, 10, 30), y: 0 },
				{ x: new Date(2019, 11, 30), y: 0 }
			]
		}]
	});
	chart.render();

}

$('#opportunity-tab').on("shown.bs.tab", function () {
	ctl();
	$('#opportunity-tab').off(); // to remove the binded event after the initial rendering
});


window.onload = function () {
	/*Chart 5*/
	var chart = new CanvasJS.Chart("loan-type",
		{
			//theme: "light2",
			colorSet: "blue",
			title: {
				text: "Loan Type",
				fontFamily: "Open Sans Condensed",
				fontWeight: "bold",
				fontColor: "#1ea1b8"
			},
			data: [{
				type: "pie",
				startAngle: 310,
				indexLabelFontColor: "#9b9b9b",
				indexLabelLineColor: "#9b9b9b",
				yValueFormatString: "(##0\"%\")",
				indexLabel: "{label} {y}",
				dataPoints: [
					{ y: 69, label: "FHA", color: "#1ea1b8" },
					{ y: 4, label: "Jumbo", color: "#9b9b9b" },
					{ y: 21, label: "Conventional", color: "#263a8e" },
					{ y: 6, label: "USDA", color: "#fdb71a" },
				]
			}]
		});
	chart.render();

	/*Chart 6*/
	var chart = new CanvasJS.Chart("loan-purpose",
		{
			//theme: "light2",
			//colorSet: "blue",
			title: {
				text: "Loan Purpose",
				fontFamily: "Open Sans Condensed",
				fontWeight: "bold",
				fontColor: "#1ea1b8"
			},
			data: [{
				type: "pie",
				startAngle: 320,
				indexLabelFontColor: "#9b9b9b",
				indexLabelLineColor: "#9b9b9b",
				yValueFormatString: "(##0\"%\")",
				indexLabel: "{label} {y}",
				dataPoints: [
					{ y: 90, label: "Purchase", color: "#1ea1b8" },
					{
						y:
							10, label: "Refinance", color: "#263a8e"
					},
					//{y: 21, label: "Conventional"},
					//{y: 6, label: "USDA"},
				]
			}]
		});
	chart.render();

	/*Chart 7*/
	var chart = new CanvasJS.Chart("loan-type1",
		{
			//theme: "light2",
			//colorSet: "blue",
			title: {
				text: "Loan Type",
				fontFamily: "Open Sans Condensed",
				fontWeight: "bold",
				fontColor: "#1ea1b8"
			},
			data: [{
				type: "pie",
				startAngle: 360,
				indexLabelFontColor: "#9b9b9b",
				indexLabelLineColor: "#9b9b9b",
				yValueFormatString: "(##0\"%\")",
				indexLabel: "{label} {y}",
				dataPoints: [
					{ y: 59, label: "FHA", color: "#1ea1b8" },
					{ y: 41, label: "Conventional", color: "#263a8e" },
					//{y: 21, label: "Conventional"},
					//{y: 6, label: "USDA"},
				]
			}]
		});
	chart.render();

	/*Chart 8*/
	var chart = new CanvasJS.Chart("loan-purpose1",
		{
			//theme: "light2",
			//colorSet: "blue",
			title: {
				text: "Loan Purpose",
				fontFamily: "Open Sans Condensed",
				fontWeight: "bold",
				fontColor: "#1ea1b8"
			},
			data: [{
				type: "pie",
				startAngle: 320,
				indexLabelFontColor: "#9b9b9b",
				indexLabelLineColor: "#9b9b9b",
				yValueFormatString: "(##0\"%\")",
				indexLabel: "{label} {y}",
				dataPoints: [
					{ y: 96, label: "Purchase", color: "#1ea1b8" },
					{ y: 4, label: "Refinance", color: "#263a8e" },
					//{y: 21, label: "Conventional"},
					//{y: 6, label: "USDA"},
				]
			}]
		});
	chart.render();

	var chart = new CanvasJS.Chart("chartContainer6", {
		title: {
			text: "Total Amount: $415,732 Count:3",
			wrap: true,
			maxWidth: 190,
			fontSize: 13,
			fontFamily: "Arial",
			fontWeight: "bold",
			fontColor: '#1ea1b8'
		},
		axisX: {
			valueFormatString: "MMM YYYY",
			interval: 1,
			intervalType: "month",
			labelFontColor: '#9c9c9c',
			labelFontWeight: "bold",
			tickLength: 20,
			tickColor: "#ffffff",

		},
		axisY: [{
			title: "Count",
			fontSize: 12,
			valueFormatString: "#,0#",
			lineColor: "#369EAD",
			lineThickness: 2,
			tickColor: "#369EAD",
			labelFontColor: "#9b9b9b",
			titleFontColor: "#9b9b9b",
			tickLength: 10,
			tickThickness: 2,
			gridDashType: "dash",
			gridColor: "#d1d1d1",
		}
		],
		axisY2: {
			title: "Amount",
			fontSize: 12,
			lineColor: "#263a8e",
			lineThickness: 2,
			tickColor: "#263a8e",
			labelFontColor: "#9b9b9b",
			titleFontColor: "#9b9b9b",
			tickLength: 10,
			tickThickness: 2,
			prefix: "$",
			suffix: "k",
			interval: 10,
			//valueFormatString: "##,0#",

			fontWeight: "bold",
		},
		toolTip: {
			shared: true
		},
		legend: {
			//cursor: "pointer",
			//itemclick: toggleDataSeries
		},
		data: [
			{
				type: "line",
				name: "Count",
				color: "#1ea1b8",
				//showInLegend: true,
				dataPoints: [
					{ x: new Date(2018, 00, 0), y: 0 },
					{ x: new Date(2018, 01, 14), y: 33.9 },
					{ x: new Date(2018, 02, 21), y: 26.0 },
					{ x: new Date(2018, 03, 28), y: 15.8 },
					{ x: new Date(2018, 04, 4), y: 18.6 },
					{ x: new Date(2018, 05, 11), y: 34.6 },
					{ x: new Date(2018, 06, 18), y: 37.7 },
					{ x: new Date(2018, 07, 25), y: 24.7 },
					{ x: new Date(2018, 08, 4), y: 35.9 },
					{ x: new Date(2018, 09, 11), y: 12.8 },
					{ x: new Date(2018, 10, 18), y: 38.1 },
					{ x: new Date(2018, 11, 25), y: 42.4 }
				]
			},
			{
				type: "line",
				name: "Amount",
				color: "#334695",
				axisYType: "secondary",
				//showInLegend: true,
				dataPoints: [
					{ x: new Date(2018, 00, 0), y: 0 },
					{ x: new Date(2018, 01, 14), y: 44.3 },
					{ x: new Date(2018, 02, 21), y: 28.7 },
					{ x: new Date(2018, 03, 28), y: 22.5 },
					{ x: new Date(2018, 04, 4), y: 25.6 },
					{ x: new Date(2018, 05, 11), y: 45.7 },
					{ x: new Date(2018, 06, 18), y: 54.6 },
					{ x: new Date(2018, 07, 25), y: 32.0 },
					{ x: new Date(2018, 08, 4), y: 43.9 },
					{ x: new Date(2018, 09, 11), y: 26.4 },
					{ x: new Date(2018, 10, 18), y: 40.3 },
					{ x: new Date(2018, 11, 25), y: 54.2 }
				]
			}]
	});
	chart.render();
}



$(document).ready(function () {
	$(".fancybox").fancybox({
		openEffect: 'none',
		closeEffect: 'none',
		autoResize: false,
		fitToView: false,
		padding: false,

	});
	$(".messaging").fancybox({
		openEffect: 'none',
		closeEffect: 'none',
		autoResize: false,
		fitToView: false,
		padding: false,
		helpers: {
			title: { type: 'over' }
		}, // helpers
		beforeShow: function () {
			this.title = (this.title ? '' + this.title + '' : '') + 'Milestone Update (' + (this.index + 1) + ' of ' + this.group.length + ')';
		} // beforeShow
	});
});

jQuery(document).ready(function () {
	jQuery('.generate').click(function (event) {
		event.preventDefault();
		jQuery('.content_section').toggleClass('visible');
		jQuery('.content_section1').toggleClass('hide');
	});
	jQuery('.import').click(function (event) {
		event.preventDefault();
		jQuery('.import_section').toggleClass('visible');
		jQuery('.content_section1').toggleClass('hide');
	});
});

jQuery(document).ready(function () {
	jQuery('.generate-letter').click(function (event) {
		event.preventDefault();
		jQuery('.generated-letter').toggleClass('visible');
		jQuery('.letter-before').toggleClass('hide');
	});

});
jQuery(document).ready(function () {
	jQuery('.calculate').click(function (event) {
		event.preventDefault();
		jQuery('.calculate-item').addClass('visible-value');
		jQuery('.generate-letter').removeClass('gray');
	});

	jQuery('.generate-letter').click(function (event) {
		event.preventDefault();
		jQuery('.inaccessible').removeClass('gray inaccessible');
	});

});

/*row remove*/

$('table').on('click', 'tr a.remove', function (e) {
	e.preventDefault();
	$(this).closest('tr').remove();
});

/*$('.default-list li').on('checked','label.custom_check input["checkbox"]',function(e){
  e.preventDefault();
  $(this).closest('li').toggleClass('checked-item');
});*/

$(".todo label.custom_check input").change(function () {
	if ($(this).is(':checked'))
		$(this).closest('li').addClass('checked-item');
	else
		$(this).closest('li').removeClass('checked-item');
});

// Javascript to enable link to tab
var url = document.location.toString();
if (url.match('#')) {
	$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
}

// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function (e) {
	if (history.pushState) {
		history.pushState(null, null, e.target.hash);
	} else {
		window.location.hash = e.target.hash; //Polyfill for old browsers
	}
});

$(document).ready(function () {
	var divs = $('.nextElements>.step-item');
	var now = 0; // currently shown div
	divs.hide().first().show();
	$("a.next").click(function (e) {
		$('.step-item').eq().addClass('active');
		$("a.back").fadeIn(700).scrollTop();
		divs.eq(now).hide().fadeOut(700).scrollTop();
		now = (now + 1 < divs.length) ? now + 1 : 0;
		divs.eq(now).fadeIn(700).scrollTop(); // show next
		jQuery('iframe[src*="https://www.youtube.com/embed/"]').addClass("youtube-iframe");
		// changes the iframe src to prevent playback or stop the video playback in our case
		$('.youtube-iframe').each(function (index) {
			$(this).attr('src', $(this).attr('src'));
			$(this).src += "?autoplay=0";
			return true;
		});

		//click function
	});
	$("a.back").click(function (e) {
		divs.eq(now).hide().scrollTop();
		now = (now > 0) ? now - 1 : divs.length - 1 - 0;
		divs.eq(now).fadeIn(1000).scrollTop(); // or .css('display','block');
		//console.log(divs.length, now);		
	});
});


$(document).ready(function () {
	var divs = $('.nextElements>.step-content');
	var now = 0; // currently shown div
	divs.hide().first().show();
	$("button[name=next]").click(function (e) {
		$('.step-content').eq().addClass('active');
		$("button[name=prev]").show();
		divs.eq(now).hide().fadeOut();
		now = (now + 1 < divs.length) ? now + 1 : 0;
		divs.eq(now).show().fadeIn(); // show next
	});
	$("button[name=prev]").click(function (e) {
		divs.eq(now).hide();
		now = (now > 0) ? now - 1 : divs.length - 1 - 0;
		divs.eq(now).show().fadeIn(); // or .css('display','block');
		//console.log(divs.length, now);

	});
});



jQuery(function ($) {
	var projects = [
		{
			value: "Jeff Brown",
			label: "Jeff Brown",
			desc: "jpbrown10@gmail.com",
			info: "972 838 6835",
			cinfo: "<span class='default-color'><a href='opportunities.html' class='default-color'>Opportunities</a> - <a href='opportunities.html#attempted' class='default-color'>Contact Attempted</a></span>"
		},
		{
			value: "Michael Savage",
			label: "Michael Savage",
			desc: "MSavage151@gmail.com",
			info: "972 838 6835",
			cinfo: "<span class='default-color'><a href='opportunities.html' class='default-color'>Opportunities</a> - <a href='opportunities.html#attempted' class='default-color'>Contact Attempted</a></span>"
		},
		{
			value: "Robert Mueller",
			label: "Robert Mueller",
			desc: "RobertMueller@hotmail.com",
			info: "972 838 6835",
			cinfo: "<span class='default-color'><a href='opportunities.html' class='default-color'>Opportunities</a> - <a href='opportunities.html#attempted' class='default-color'>Contact Attempted</a></span>"
		}
	];
	$(function () {
		$("#userinfo,#contactAssociation").autocomplete({
			minLength: 0,
			source: projects,
			focus: function (event, ui) {
				$("#userinfo,#contactAssociation").val(ui.item.label);
				return false;
			},
			select: function (event, ui) {
				$("#userinfo").val(ui.item.label);

				return false;
			}
		}),
			$("#userinfo,#contactAssociation").autocomplete("instance")._renderItem = function (ul, item) {
				return $("<li>")
					.append("<div><strong>" + item.label + "</strong><br>" + item.desc + "<br>" + item.info + "<br>" + item.cinfo + "</div>")
					.appendTo(ul);
			};
	})
});


jQuery(function ($) {
	var searches = [
		{
			value: "Jeff Brown",
			label: "Jeff Brown",
			desc: "jpbrown10@gmail.com",
			info: "972 838 6835",
			cinfo: "<span class='default-color'><a href='opportunities.html' class='default-color'>Opportunities</a> - <a href='opportunities.html#applications' class='default-color'>Completed Application</a></span>"
		},
		{
			value: "Jeff Brownstonian",
			label: "Jeff Brownstonian",
			desc: "jeffreytwin@yahoo.com",
			info: "912. 127. 5811",
			cinfo: "<span class='default-color'><a href='opportunities.html' class='default-color'>Opportunities</a> - <a href='opportunities.html#attempted' class='default-color'>Contact Attempted</a></span>"
		},

	];

	$("#searchInfo").autocomplete({
		minLength: 0,
		source: searches,
		focus: function (event, ui) {
			$("#searchInfo").val(ui.item.label);
			return false;
		},
		select: function (event, ui) {
			$("#searchInfo").val(ui.item.label);

			return false;
		}
	})
		.autocomplete("instance")._renderItem = function (ul, item) {
			return $("<li>")
				.append("<div><strong>" + item.label + "</strong><br>" + item.desc + "<br>" + item.info + "<br>" + item.cinfo + "</div>")
				.appendTo(ul);
		};
});

/* $(document).ready(function(e) {
   $('.dateselectdropdown .comiseo-daterangepicker-triggerbutton').append('<span>Due: </span>');
});*/


$(document).ready(function (e) {
	$(".datepickertab li div").click(function () {
		$(this).parent().addClass("active");
		$(this).parent().siblings().removeClass("active");
	});
});

$(".range").daterangepicker({
	//dateFormat:"10/12/2012", 
	//initialText : 'Due:',
	initialText: 'Today',
	presetRanges: [{
		text: 'Today',
		dateStart: function () { return moment() },
		dateEnd: function () { return moment() }
	}, {
		text: 'Tomorrow',
		dateStart: function () { return moment().add('days', 1) },
		dateEnd: function () { return moment().add('days', 1) }
	}, {
		text: 'This Week',
		dateStart: function () { return moment() },
		dateEnd: function () { return moment().add('days', 7) }
	}, {
		text: 'All Time',
		dateStart: function () { return moment().add('weeks', 1).startOf('week') },
		dateEnd: function () { return moment().add('month', 12).endOf('week') }
	}],
	applyOnMenuSelect: false,
	datepickerOptions: {
		//minDate: -20,
		maxDate: null,
		numberOfMonths: 2
	}
});


$(".previous-range").daterangepicker({
	customClass: 'some-css-class',
	//dateFormat:"10/12/2012", 
	//initialText : 'Due:',
	presetRanges: [

		{ text: 'This Month', dateStart: function () { return moment().startOf('month') }, dateEnd: function () { return moment().endOf('month') } },
		{ text: 'Last Month', dateStart: function () { return moment().subtract('month', 1).startOf('month') }, dateEnd: function () { return moment().subtract('month', 1).endOf('month') } },
		{ text: 'Last Quarter', dateStart: function () { return moment().subtract('month', 3).startOf('days', 1) }, dateEnd: function () { return moment().subtract('month').endOf('quarter') } },
		{ text: 'Last Year', dateStart: function () { return moment().subtract('year').startOf('year') }, dateEnd: function () { return moment().subtract('year').endOf('year') } },
		{ text: 'All Time', dateStart: function () { return moment().startOf('year') }, dateEnd: function () { return moment() } }
	],
	initialText: 'This Month',
	applyOnMenuSelect: false,
	datepickerOptions: {
		start: 'yesterday',
		maxDate: new Date(),
		numberOfMonths: 2
	}
});

$(document).ready(function () {
	$('.dropdown-menu .dropdown button').on("click", function (e) {
		$(this).next('.dropdown-menu').toggle();
		e.stopPropagation();
		//e.preventDefault();	
		return false
	});
});
$('.dropdown-menu .dropdown li').each(function () {
	$(this).click(function () {
		$('.selected').html($(this).html());
	})

});
/* $('.dropdown-menu .hasDatepicker a').on('click', function(e){
	$('.dropdown-menu').toggle();
	   e.stopPropagation();
   //e.preventDefault();	
	   return false
   
 });*/

$(window).on('load', function () {
	$('#welcome-modal').modal('show');
});
/*$('.next').click(function(e){
	e.preventDefault();
   $(this).parent().hide().next().show();//hide parent and show next
    $(".modal").scrollTop(0);
});
*/


function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

jQuery(function ($) {
	$(".phone").mask("(999) 999-9999");
});


$(document).ready(function () {

	var MaxInputs = 2; //maximum extra input boxes allowed
	var InputsWrapper = $("#secondaryEmail"); //Input boxes wrapper ID
	var AddButton = $("#AddMoreFileBox"); //Add button ID

	var x = InputsWrapper.length; //initlal text box count
	var FieldCount = 0; //to keep track of text box added

	//on add input button click
	$(AddButton).click(function (e) {
		//max input box allowed
		if (x <= MaxInputs) {
			FieldCount++; //text box added ncrement
			//add input box
			$(InputsWrapper).append('<div class="bold gray mb-3 row align-items-center"><div class="col-md-7">Secondary Email ' + FieldCount + ' for Notifications: <div class="position-relative notepopup d-inline-flex"><span class="notification">i</span> <div class="popover default-color"><p class="bold">What is this?</p><p>This is an email address that will receive any email notifications that your primary email will receive. This can be useful for directing lead notifications to your support staff.</p></div></div></div> <div class="col-md-5"><input class="default-input default-color block" type="email" placeholder="" required></div></div>');

			x++; //text box increment

			$("#AddMoreFileId").show();

			$('AddMoreFileBox').html("Add field");

			// Delete the "add"-link if there is 3 fields.
			if (x == 3) {
				$("#AddMoreFileId").hide();
				$("#lineBreak").html("<br>");
			}
		}
		return false;
	});

	$("body").on("click", ".removeclass", function (e) { //user click on remove text
		if (x > 1) {
			$(this).parent('div').remove(); //remove text box
			x--; //decrement textbox

			$("#AddMoreFileId").show();

			$("#lineBreak").html("");

			// Adds the "add" link again when a field is removed.
			$('AddMoreFileBox').html("Add field");
		}
		return false;
	})

});

$(document).ready(function (e) {
	$('#welcome-modal').modal({ backdrop: 'static', keyboard: false })
});

jQuery(document).ready(function () {
	jQuery('.view-complete').click(function (e) {
		e.preventDefault();
		jQuery(this).toggleClass('selected-view');

		if (jQuery('.view-complete').hasClass('selected-view')) { //existing class
			jQuery('.view-complete span').text('Hide Completed');
		} else {
			jQuery('.view-complete span').text('View Completed');
		}
	});

});

/*jQuery('.dropdown-toggle').on('click', function (e) {
  $(this).next().toggle();
});
jQuery('.dropdown-menu.allow-focus').on('click', function (e) {
  e.stopPropagation();
});
*/


jQuery(document).ready(function ($) {
	$('input[name="all"]').bind('click', function () {
		var status = $(this).is(':checked');
		$('input[type="checkbox"]', $('li')).attr('checked', status);
	});
	$('.clear-filter').on('click', function (e) {
		e.preventDefault();
		$('input[type="checkbox"]').removeAttr('checked');
	})


	$('#selectAll').click(function (e) {
		$(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
	});


});
/*jQuery(document).ready(function($) {
$('.nav-tabs a').click(function () {
    $(this).tab('show');
    $("<a>").data("target", $(this).data("second-tab")).tab("show")
})
})*/

// $(document).ready(function () {
// 	$(".texteditor").Editor();
// 	$(".texteditor2").Editor();
// });


$('.right-dropdown a').on("click", function (e) {
	// e.preventDefault();
	$(this).parent('.right-dropdown').siblings().children('.dropdown-menu').fadeOut();
	$(this).siblings('.dropdown-menu').fadeToggle();
	$(this).children('.right-dropdown').toggleClass('show');
});

$(document).mouseup(function (e) {
	var container = $(".right-dropdown .dropdown-menu");

	if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
	{
		container.hide();
	}
});

/*
$(document).ready(function() {
    $(".nav-item a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-pane").not(tab).css("display", "none");
        $(tab).fadeIn();
		$(tab).addClass("current");
        $(tab).siblings().removeClass("current");
    });
});*/


$(document).ready(function () {
	//$(".address-info").hide();
	$(".address_same").click(function () {
		if ($(this).is(":checked")) {
			$(".address-info").addClass('activated');
		} else {
			$(".address-info").removeClass('activated');
		}
	});
	/*$(".btn-claim").each(function(e) {
		e.preventDefault();
	   $(".actions").removeClass('hide');
	   $(this).addClass('hide');
	});*/
});

jQuery(document).ready(function () {
	jQuery(".btn-claim").click(function (e) {
		e.preventDefault();
		jQuery(this).toggleClass("hide");
		jQuery(this).parent().toggleClass("active-action");
		jQuery(this).siblings().removeClass("hide");
	})
});


$(document).ready(function () {
	// Load the first 3 list items from another HTML file
	//$('#myList').load('externalList.html li:lt(3)');
	$('.checkbox-menu li:lt(5)').show();
	$('.see_more').click(function (e) {
		e.preventDefault();
		$('.checkbox-menu li:lt(50)').show();
	});
	$('.showLess').click(function () {
		$('.myList li').not(':lt(3)').hide();
	});
});

jQuery(document).ready(function () {
	jQuery(".notepopup").mouseover(function (e) {
		jQuery('iframe[src*="https://www.youtube.com/embed/"]').addClass("youtube-iframe");
		// changes the iframe src to prevent playback or stop the video playback in our case
		$('.youtube-iframe').each(function (index) {
			$(this).attr('src', $(this).attr('src'));
			$(this).src += "?autoplay=0";
			return true;
		});
	});
});


/*Multiple Tabs javascript*/

/*End Multiple Javascript*/

/*
$(document).ready(function() {
    $("ul.nav-tabs li a").on('click',function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-pane").not(tab).css("display", "none");
        $(tab).fadeIn();
		$(tab).addClass("current");
        $(tab).siblings().removeClass("current");
    });

});
*/
/*
jQuery(document).ready(function () {
function showDiv(divId, element)
{
    document.getElementById(divId).style.display = element.value == 1 ? 'block' : 'none';
}
});
*/

$('.attempt-wrap').on('click', '.remove', function () {
	$('.remove').closest('.attempt-wrap').find('.attempt-element').not(':first').last().remove();
});
$('.attempt-wrap').on('click', '.clone', function () {
	$('.clone').closest('.attempt-wrap').find('.attempt-element').first().clone().appendTo('.results');

});
jQuery(".attempt-element .count").append().after(" <span class='remove gray text11'>(delete)</span>");

$(document).ready(function () {

	$('ul.opportunities_tabs li a').click(function (e) {
		e.preventDefault();
		var tab_id = $(this).attr('data-tab');
		//var data_id = $(this).attr('data-id');
		console.log(tab_id);

		$('ul.opportunities_tabs li a').removeClass('active');
		//$('.tab-pane').removeClass('active show');

		$('.opportunities-tab-content .tab-pane').hide();
		//$('.tab-content >.tab-pane:first').show();
		$(".tab-pane").fadeOut(800);
		//$('.tab-content >.tab-pane').removeClass('active show');

		//$(".container div[data-id="+tab_id+"]").addClass('active show');

		$(".container div[data-id=" + tab_id + "]").fadeIn(800);
		//$(".container div[data-id="+tab_id+"]").addClass('active show');
		$('ul.opportunities_tabs li a[data-tab=' + tab_id + ']').addClass('active');
	});


	$('ul.simple-tabs li a').click(function (e) {
		e.preventDefault();
		var tab_id = $(this).attr('data-tab');
		//var data_id = $(this).attr('data-id');
		console.log(tab_id);

		$('ul.simple-tabs li a').removeClass('active');
		//$('.tab-pane').removeClass('active show');

		$('.simple-tab-content .tab-pane').hide();
		//$('.tab-content >.tab-pane:first').show();
		$(".tabs-pane").fadeOut(800);
		//$('.tab-content >.tab-pane').removeClass('active show');

		//$(".container div[data-id="+tab_id+"]").addClass('active show');

		$(".simple-tab-content .tabs-pane[data-id=" + tab_id + "]").fadeIn(500);
		//$(".container div[data-id="+tab_id+"]").addClass('active show');
		$('ul.simple-tabs li a[data-tab=' + tab_id + ']').addClass('active');
	});

	$('.currency').on('input', function () {
		var val = $(this).val(),
			arr = val.split('.');

		if (arr.length > 1)
			$(this).val(arr[0] + val.substr(val.length - 1) + '.00')
		else
			$(this).val('$' + val + '.00')
	}).on('keypress', function (e) {
		return e.keyCode == 8 || e.charCode == 46 || (e.charCode >= 48 && e.charCode <= 57);
		return false;
	})

})



$(document).on('click', '.dropdown-menu.sort-dropdown', function (e) {
	e.stopPropagation();
});


$(document).ready(function () {
	$('[id^=branchCode]').keypress(validateNumber);
});

function validateNumber(event) {
	var key = window.event ? event.keyCode : event.which;
	if (event.keyCode === 8 || event.keyCode === 46) {
		return true;
	} else if (key < 48 || key > 57) {
		return false;
	} else {
		return true;
	}
};


var image = new Array();
image[0] = "images/nothing.png";
image[1] = "images/nothing2.png";
image[2] = "images/nothing1.png";
image[3] = "images/nothing3.png";
var size = image.length
var x = Math.floor(size * Math.random())

jQuery('#random').attr('src', image[x]);


$(document).ready(function () {
	$(".pull-button").on("click", function (e) {
		e.preventDefault();
		var c = $(".slide-menu");
		var j = $(".slide-menu").width();
		c.toggleClass("open");
		if (c.hasClass("open")) {
			c.animate({
				right: "0px"
			})

		} else {
			c.animate({
				right: -j + 50 + "px"
			}, 280)
		}
	})

});


jQuery(document).ready(function () {
	$('.editable-items li span:last-child').append('<span class="editfield"></span>');


	$('.editable-items li').hover(function () {
		$('.editfield', this).css('display', 'inline-block')
	}, function () {
		$('.editfield', this).css('display', 'none')
	})
});
//$('.editable-items li.noteditable span:last-child').find('<span class="editfield"></span>').remove();
jQuery(document).ready(function () {
	jQuery(".editfield").click(function (e) {
		e.preventDefault();
		//jQuery('.editable-items').addClass('edit-active');
		jQuery('.editable-items').removeClass('editable-form');
		jQuery('.saveform').show();
		jQuery('.checkedit').show();
		jQuery('.editfield').addClass('hidden');
		jQuery('.custom_radio').addClass('activated_radio');
	})

	jQuery(".saveform").click(function (e) {
		e.preventDefault();
		jQuery('.editable-items').addClass('edit-active');
		jQuery('.editable-items').addClass('editable-form');
		jQuery('.saveform').hide();
		jQuery('.checkedit').hide();
		jQuery('.editfield').removeClass('hidden');
		jQuery('.custom_radio').removeClass('activated_radio');
		jQuery('.custom_radio').hide();
	})
});


$(function () {
	var $radioButtons = $('.custom_radio input[type="radio"]');
	$radioButtons.click(function () {
		$radioButtons.each(function () {
			$(this).parent().toggleClass('selected', this.checked).removeClass('unselected');
		});
	});
});



/* currency function*/

// Jquery Dependency

$("input[data-type='currency']").on({
	keyup: function () {
		formatCurrency($(this));
	},
	blur: function () {
		formatCurrency($(this), "blur");
	}
});


function formatNumber(n) {
	// format number 1000000 to 1,234,567
	return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
	// appends $ to value, validates decimal side
	// and puts cursor back in right position.

	// get input value
	var input_val = input.val();

	// don't validate empty input
	if (input_val === "") { return; }

	// original length
	var original_len = input_val.length;

	// initial caret position 
	var caret_pos = input.prop("selectionStart");

	// check for decimal
	if (input_val.indexOf(".") >= 0) {

		// get position of first decimal
		// this prevents multiple decimals from
		// being entered
		var decimal_pos = input_val.indexOf(".");

		// split number by decimal point
		var left_side = input_val.substring(0, decimal_pos);
		var right_side = input_val.substring(decimal_pos);

		// add commas to left side of number
		left_side = formatNumber(left_side);

		// validate right side
		right_side = formatNumber(right_side);

		// On blur make sure 2 numbers after decimal
		if (blur === "blur") {
			right_side += "00";
		}

		// Limit decimal to only 2 digits
		right_side = right_side.substring(0, 2);

		// join number by .
		input_val = "$" + left_side + "." + right_side;

	} else {
		// no decimal entered
		// add commas to number
		// remove all non-digits
		input_val = formatNumber(input_val);
		input_val = "$" + input_val;

		// final formatting
		if (blur === "blur") {
			input_val += ".00";
		}
	}

	// send updated string to input
	input.val(input_val);

	// put caret back in the right position
	var updated_len = input_val.length;
	caret_pos = updated_len - original_len + caret_pos;
	input[0].setSelectionRange(caret_pos, caret_pos);
}



/*Percentage function*/

$("input[data-type='percentage']").on({
	keyup: function () {
		formatPercentage($(this));
	},
	blur: function () {
		formatPercentage($(this), "blur");
	}
});


function formatNumber(n) {
	// format number 1000000 to 1,234,567
	return n.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ",")
}


function formatPercentage(input, blur) {
	// appends $ to value, validates decimal side
	// and puts cursor back in right position.

	// get input value
	var input_val = input.val();

	// don't validate empty input
	if (input_val === "") { return; }

	// original length
	var original_len = input_val.length;

	// initial caret position 
	var caret_pos = input.prop("selectionStart");

	// check for decimal
	if (input_val.indexOf(".") >= 0) {

		// get position of first decimal
		// this prevents multiple decimals from
		// being entered
		var decimal_pos = input_val.indexOf(".");

		// split number by decimal point
		var left_side = input_val.substring(decimal_pos, 0);
		var right_side = input_val.substring(decimal_pos);

		// add commas to left side of number
		left_side = formatNumber(left_side);

		// validate right side
		right_side = formatNumber(right_side);

		// On blur make sure 2 numbers after decimal
		if (blur === "blur") {
			right_side += "00";
		}

		// Limit decimal to only 2 digits
		right_side = right_side.substring(0, 2);

		// join number by .
		input_val = "%" + left_side + "." + right_side;

	} else {
		// no decimal entered
		// add commas to number
		// remove all non-digits
		input_val = formatNumber(input_val);
		input_val = input_val + "%";

		// final formatting
		if (blur === "blur") {
			input_val += ".00";
		}
	}

	// send updated string to input
	input.val(input_val);

	// put caret back in the right position
	var updated_len = input_val.length;
	caret_pos = updated_len - original_len + caret_pos;
	input[0].setSelectionRange(caret_pos, caret_pos);
}





