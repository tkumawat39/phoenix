/* Developer by Mohit Chawla */

var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

$(document).ready(function() {
	$( "#loginForm" ).validate({
		rules: {
			username: {
				required: true,
				maxlength: 250
			},
			password: {
				required: true,
				maxlength: 100,
				password:true
			}
		},
		errorPlacement: function ( error, element ) {
			error.addClass( "ui red pointing label transition" );
			error.insertAfter( element.parent() );
		}
	});
	
	
	// creating pagination and fetching data 
	
	$(document).on('click', '.pagination a',function(event)
	{
		event.preventDefault();
		$('li').removeClass('active');
		$(this).parent('li').addClass('active');
		var myurl = $(this).attr('href');
		var page=$(this).attr('href').split('page=')[1];
		getData(page);
	});
	
	$.ajaxSetup({
	  headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});
	
	$( "#userForm" ).validate({
		rules: {
			first_name: {
				required: true,
				maxlength: 250
			},
			last_name: {
				required: true,
				maxlength: 250
			},
			contact: {
				required: true,
				maxlength: 250,
				number:true
			},
			username: {
				required: true,
				maxlength: 250
			},
			email: {
				required: true,
				maxlength: 250,
				email:true
			},
			password: {
				required: true,
				maxlength: 100,
			},
			role: {
				required: true,
			}
		},
		errorPlacement: function ( error, element ) {
			error.addClass( "ui red pointing label transition" );
			error.insertAfter( element.parent() );
		}
	});
	
	$( "#accountForm" ).validate({
		rules: {
			first_name: {
				required: true,
				maxlength: 250
			},
			last_name: {
				required: true,
				maxlength: 250
			},
			contact: {
				required: true,
				maxlength: 250,
				number:true
			},
			username: {
				required: true,
				maxlength: 250
			},
			email: {
				required: true,
				maxlength: 250,
				email:true
			}
		},
		errorPlacement: function ( error, element ) {
			error.addClass( "ui red pointing label transition" );
			error.insertAfter( element.parent() );
		}
	});
	
	$( "#pwdForm" ).validate({
		rules: {
			old_password: {
				required: true,
				minlength: 5
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			}
		},
		errorPlacement: function ( error, element ) {
			error.addClass( "ui red pointing label transition" );
			error.insertAfter( element.parent() );
		}
	});
	
	$( "#userEditForm" ).validate({
		rules: {
			first_name: {
				required: true,
				maxlength: 250
			},
			last_name: {
				required: true,
				maxlength: 250
			},
			contact: {
				required: true,
				maxlength: 250,
				number:true
			},
			username: {
				required: true,
				maxlength: 250
			},
			email: {
				required: true,
				maxlength: 250,
				email:true
			},
			role: {
				required: true,
			}
		},
		errorPlacement: function ( error, element ) {
			error.addClass( "ui red pointing label transition" );
			error.insertAfter( element.parent() );
		}
	});
	
	$( "#userPwdForm" ).validate({
		rules: {
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			}
		},
		errorPlacement: function ( error, element ) {
			error.addClass( "ui red pointing label transition" );
			error.insertAfter( element.parent() );
		}
	});
	
});

// creating pagination and fetching data 

function getData(page){
	
	var keyword_search = $('#keyword_search').val();
	
	
	$.ajax(
	{
		url: '?page=' + page+'&keyword_search='+keyword_search,
		type: "get",
		datatype: "html",
	}).done(function(data){
		$("#tag_container").empty().html(data);
	}).fail(function(jqXHR, ajaxOptions, thrownError){

		  //alert('No response from server');
	});
}

$(document).on('keyup','#keyword_search',function(e){
   getData(0);
});

$(document).on('click','.change_user_status',function(e){
   var userId = $(this).attr('data-attr');
	
	if($(this).is(":checked")){
		var status = 'Active';
	} else {
		var status = 'Inactive';
	}
	
	$.ajax
	({
		type: "POST",
		url:baseUrl+'/admin/change_user_status',
		data: {'userId':userId,'status':status},
		success: function(response)
		{
			
		},
		error: function(response)
		{
			
		},
	});
});

$(document).on('click','.user_account_open',function(e){
   var userId = $(this).attr('data-attr');
	
	$this = $(this);
	$.ajax
	({
		type: "POST",
		url:baseUrl+'/admin/user_account_open',
		data: {'userId':userId},
		success: function(response)
		{
			if(response=='success'){ $this.removeClass('user_account_open');  $this.find('i.icon-lock').removeClass('icon-lock').addClass('icon-unlock'); }
		},
		error: function(response)
		{
			
		},
	});
});


function edit_users(e){
	
	$('#userId').val($(e).attr('data-id'));
	$('#first_name').val($(e).attr('data-first_name'));
	$('#last_name').val($(e).attr('data-last_name'));
	$('#contact').val($(e).attr('data-contact'));
	$('#username').val($(e).attr('data-username'));
	$('#email').val($(e).attr('data-email'));
	$('#role').val($(e).attr('data-role'));
	
	$('#edit-modal').modal('show');
}

function reset_password(e){
	
	$('#user_Id').val($(e).attr('data-id'));
	
	$('#modal-title').html('Reset Password - '+ $(e).attr('data-first_name') + ' '+$(e).attr('data-last_name'));
	
	$('#reset-modal').modal('show');
}


