import { Component, ViewEncapsulation, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-call-contact-dialog',
  templateUrl: './call-contact-dialog.component.html',
  styleUrls: ['./call-contact-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CallContactDialogComponent  {

  constructor(
    public dialogRef: MatDialogRef<CallContactDialogComponent>,
  ) {}

  close(): void {
    this.dialogRef.close();
  }
}
