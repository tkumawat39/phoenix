import { Component, ViewEncapsulation, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-cancel-loan-dialog',
  templateUrl: './cancel-loan-dialog.component.html',
  styleUrls: ['./cancel-loan-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CancelLoanDialogComponent  {

  constructor(
    public dialogRef: MatDialogRef<CancelLoanDialogComponent>,
  ) {}

  close(): void {
    this.dialogRef.close();
  }

}

