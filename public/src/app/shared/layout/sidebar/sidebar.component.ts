import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {


  constructor() { }

  ngOnInit() {
  }

  sideMenu() {
    document.getElementsByClassName('slide-menu')[0].classList.toggle('open');
  }

}
