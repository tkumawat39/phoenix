// tslint:disable-next-line:no-

/**
 * Api Route constants
 */
export namespace ApiConstants {
    export const borrower = 'borrower/1';
    export const importBorrower = 'import-borrower-to-calyx-point';
    export const leads = 'getleads'
}