import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { routing } from './app.routing';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { PagesComponent } from './pages/pages.component';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { HeaderComponent } from './shared/layout/header/header.component';
import { OpportunitiesComponent } from './modules/opportunities/opportunities.component';
import { ViewAllComponent } from './modules/borrower/view-all/view-all.component';
import { FundedComponent } from './modules/borrower/funded/funded.component';
import { ClosingComponent } from './modules/borrower/closing/closing.component';
import { UnderwritingComponent } from './modules/borrower/underwriting/underwriting.component';
import { PreSubmissionComponent } from './modules/borrower/pre-submission/pre-submission.component';
import { BorrowerPipelineComponent } from './modules/borrower/borrower-pipeline.component';
import { LoginComponent } from './core/components/auth/login/login.component';
import { AuthComponent } from './core/components/auth/auth.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { MenuComponent } from './shared/layout/menu/menu.component';
import { TodoPipelineComponent } from './modules/home/todo-pipeline/todo-pipeline.component';
import { TodoOpportunitiesComponent } from './modules/home/todo-opportunities/todo-opportunities.component';
import { HomeComponent } from './modules/home/home.component';
import { AuthGuard } from './core/guards/auth.guard';
import { AuthService } from './core/components/auth/auth.service';
import { UserService } from './modules/user/user.service';
import { EditComponent } from './modules/opportunities/edit/edit.component';
import { UncontactedComponent } from './modules/opportunities/uncontacted/uncontacted.component';
import { SidebarComponent } from './shared/layout/sidebar/sidebar.component';
import { EditMenuComponent } from './modules/opportunities/edit-menu/edit-menu.component';
import { LeadInformationComponent } from './modules/opportunities/lead-information/lead-information.component';
import { MyHttpInterceptor } from './core/interceptors/my-http-interceptor';
import { LoaderComponent } from './shared/loader/loader.component';
@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    HomeComponent,
    TodoOpportunitiesComponent,
    TodoPipelineComponent,
    FooterComponent,
    HeaderComponent,
    MenuComponent,
    NotFoundComponent,
    AuthComponent,
    LoginComponent,
    EditComponent,
    BorrowerPipelineComponent,
    PreSubmissionComponent,
    OpportunitiesComponent,
    UnderwritingComponent,
    ClosingComponent,
    FundedComponent,
    ViewAllComponent,
    UncontactedComponent,
    SidebarComponent,
    EditMenuComponent,
    LeadInformationComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    NgbModule
  ],
  providers: [
    UserService,
    AuthService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
