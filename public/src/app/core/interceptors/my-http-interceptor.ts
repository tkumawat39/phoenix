import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable, EMPTY, throwError } from 'rxjs';
import 'rxjs/add/operator/catch';

/** Defining interceptor class for transforming req & res */
@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
    constructor() { }
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // if (localStorage.getItem('SessionToken')) {
        const authReq = req.clone({
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        });
        return next.handle(authReq)
            .catch((err: any) => {
                if (err) {
                    return throwError(err);
                }
            });
        // } else {
        // }
    }
}
