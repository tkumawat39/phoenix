import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { CommonConstants } from 'src/app/config/constants';

@Injectable()
export class AuthService {

    constructor(private http: HttpClient) { }

    login(username: string, password: string) {

        const url = CommonConstants.baseUrl + '/user/authenticate';
        return this.http.post<any>(url, { username: { username }, password: { password } })
            .pipe(map(user => {
                if (user) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
    }
}
