import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {

  sub: Subscription;
  // user:any =  {username:"", password:""};
  user: any = { id: 0, username: '', firstName: '', lastName: '', email: '' };


  constructor(
    private router: Router,
    private authService: AuthService

  ) { }


  ngOnInit() {
    // $('body').addClass('dffffff');
  }

  onAddUserSubmit() { }


  /* function onLoginSubmit
  * @purpose: for authentication purpose
  * @version: 1.0.1
  * @author: bhanuwhite
  */
  onLoginSubmit() {
    console.log(this.user);

    // this.authService.login(this.user.username, this.user.password).subscribe(
    //     data => {
    //       this.router.navigate(['/home']);
    //     },
    //     error => {});

    // comment this two line code when we use api for validating user and uncommnet above two code
    localStorage.setItem('currentUser', JSON.stringify(this.user));
    this.router.navigate(['/home']);

  }

  ngOnDestroy() {
  }


}
