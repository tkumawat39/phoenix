import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html'
})

export class AuthComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit() {
   }

  ngOnDestroy() {
  }
}
