import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user/user.service';
import { ApiConstants } from '../../../../app/config/api.constants';
import { of } from 'rxjs';

var $: any;
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  borrowerData: any;
  loanNumber: any;
  constructor(private userService: UserService) {
  }
  message: string;
  isLoading: boolean = false;
  ngOnInit() {
  }

  isImportAppSuccess: boolean = false;
  /* function importApplication
        * @purpose: Importing the Application
        * @version: 1.0.1
        * @author: bhanuwhite
     */
  importApplication() {
    try {
      this.isLoading = true;
      this.userService.post(ApiConstants.importBorrower, this.borrowerData).subscribe((resp: any) => {
        if (resp.Success) {
          this.isLoading = false;
          this.loanNumber = resp.Data.Info.Attributes.FileName;
          this.isImportAppSuccess = true;
        } else {
          this.isLoading = false;
          this.isImportAppSuccess = true;
        }
      }, error => {
        this.isImportAppSuccess = true
        this.isLoading = false;
        console.log("error at import application API calling", error);
      })
    } catch (err) {
      console.log(err);
    }
  }

  receiveMessage(message: any) {
    this.borrowerData = message;

  }
}
