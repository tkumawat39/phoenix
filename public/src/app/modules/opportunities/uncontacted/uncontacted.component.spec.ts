import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UncontactedComponent } from './uncontacted.component';

describe('UncontactedComponent', () => {
  let component: UncontactedComponent;
  let fixture: ComponentFixture<UncontactedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UncontactedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UncontactedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
