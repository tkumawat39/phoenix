import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';


import { User } from '../borrower/models/user';
import { UserService } from '../user/user.service';
import { ApiConstants } from 'src/app/config/api.constants';


@Component({
  selector: 'app-opportunities',
  templateUrl: './opportunities.component.html'
})
export class OpportunitiesComponent implements OnInit {
  isLoading: boolean = false;
  sub: Subscription;
  users: User[] = [];
  uncontactedData: any;
  records: any;
  leadsData: any;
  user: User = { id: 0, firstName: '', lastName: '', email: '' };
  editFlag = false;
  constructor(private userService: UserService) { }

  ngOnInit() {
    //this.getUser();
    this.getLeads();

    this.records = [
      { id: 1, value: 10 },
      { id: 2, value: 20 },
      { id: 3, value: 50 },
      { id: 4, value: 100 },
      { id: 5, value: 500 }
    ];
  }

  onAddUserSubmit() {
    if (this.editFlag === false) {
      this.sub = this.userService.addUser(this.user).subscribe((data: any) => {
        if (data.success) {
          //this.getUser();
        } else {
          console.log('Failed');
        }
      });
    } else {
      this.sub = this.userService.updateUser(this.user).subscribe((data: any) => {
        if (data.success) {
          //this.getUser();
          this.resetForm();
        } else {
          console.log('Failed');
        }
      });
    }

  }


  onEditUser(id: number, pUser: User) {
    this.user = pUser;
    this.editFlag = true;
  }

  onDeleteUser(pId: number) {
    this.sub = this.userService.deleteUser(pId).subscribe((data: any) => {
      if (data.success) {
        //this.getUser();
      } else {
        console.log('Failed');
      }
    });
  }

  resetForm() {
    this.user = { id: 0, firstName: '', lastName: '', email: '' };
    this.editFlag = false;
  }

  // ngOnDestroy() {
  //   this.sub.unsubscribe();
  //   // need to unsubscribe of subscription of obserable to avoid memory leak. it's good practice
  // }

  // private getUser() {

  //   // this.sub = this.userService.getUsers().subscribe((data : any)=>{
  //   //   this.users = data;
  //   // });

  //   this.sub = this.userService.getUsers().subscribe((data: User[]) => {
  //     this.users = data;
  //   });

  // }

  /* function getLeads
      * @purpose: Fetching the leads
      * @version: 1.0.1
      * @author: bhanuwhite
   */
  getLeads() {
    try {
      this.isLoading = true;
      this.userService.get(ApiConstants.leads).subscribe((resp: any) => {
        this.isLoading = false;
        if (resp && resp.leads && resp.leads.length > 0) {
          this.leadsData = resp.leads;
        } else {
          this.leadsData = [];
        }
      }, err => {
        console.log(err);
      });
    } catch (err) {
      console.log(err);
    };
  }
}
