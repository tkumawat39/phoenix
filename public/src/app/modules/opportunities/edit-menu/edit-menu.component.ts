import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../user/user.service';
import { ApiConstants } from '../../../../app/config/api.constants'

@Component({
  selector: 'app-edit-menu',
  templateUrl: './edit-menu.component.html',
  styleUrls: ['./edit-menu.component.css']
})
export class EditMenuComponent implements OnInit {
  message: string = "Hola Mundo!"
  isLoading: boolean = false;
  @Output() messageEvent = new EventEmitter<string>();
  constructor(private userService: UserService) { }
  borrowerInfo: any;
  maritialStatus: any;
  years: any;
  dependents: any;
  coborrower: any;
  coborrowerValidationRemaining: number;
  validationremaining: number;
  model: any = {};
  states: any;
  houseType: any;
  loanData: any;
  ngOnInit() {
    this.getBorrowerData(ApiConstants.borrower);
    this.getDependents();
    this.getMaritialStatusValues();
    this.getStates();
    this.getHousesTypes();
    this.getYears();
    this.getLoanInfo();
  }

  showDropdownList(event) {
    console.log("sdf", event);
    // $(this).toggleClass("show-list");
    // event.target.classList.toggle('show-list');
  }

  getLoanInfo() {
    this.loanData = [{
      value: 'Select One'
    },
    {
      value: 'AL'
    },
    { value: 'AK' },
    { value: 'AR' }
    ]
  }


  /* function getMaritialStatusValues
       * @purpose: passing the maritial status values
       * @version: 1.0.1
       * @author: bhanuwhite
    */
  getMaritialStatusValues() {
    this.maritialStatus = [
      { value: 'Married / Registered Domestic Partners', id: 0 },
      { value: 'Unmarried / Single / Divorced / Widowed', id: 1 },
      { value: 'Separated', id: 2 }
    ];
  }

  /* function getStates
     * @purpose: passing the dependent valuess
     * @version: 1.0.1
     * @author: bhanuwhite
  */
  getStates() {
    this.states = [
      { value: "AL", id: '1' },
      { value: "AK", id: '2' },
      { value: "AR", id: '3' },
      { value: "AZ", id: '4' },
      { value: "CA", id: '5' },
      {
        value: "CT", id: '6'
      }
    ]
  }


  getHousesTypes() {
    this.houseType = [
      { value: "Own" },
      { value: "Rent" }
    ]
  }




  /* function getDependents
     * @purpose: passing the dependent valuess
     * @version: 1.0.1
     * @author: bhanuwhite
  */
  getDependents() {
    this.dependents = [
      { value: '1' },
      { value: '2' },
      { value: '3' },
      { value: '4' },
      { value: '5' },
      { value: '6' },
      { value: '7' },
      { value: '8' },
      { value: '9' },
      { value: '10' },
      { value: '11' },
      { value: '12' },
      { value: '13' },
      { value: '14' },
      { value: '15' }
    ];
  }


  /* function getYears
       * @purpose: passing the years values
       * @version: 1.0.1
       * @author: bhanuwhite
    */
  getYears() {
    this.years = [
      { value: '1 Year' },
      { value: '2 Years' },
      { value: '3 Years' },
      { value: '4 Years' },
      { value: '5 Years' },
      { value: '6 Years' },
      { value: '7 Years' },
      { value: '8 Years' },
      { value: '9 Years' },
      { value: '10 Years' },
      { value: '11 Years' },
      { value: '12 Years' },
      { value: '13 Years' },
      { value: '14 Years' },
      { value: '15 Years' },
      { value: '16 Years' },
      { value: '17 Years' },
      { value: '18 Years' },
      { value: '19 Years' },
      { value: '20 Years' },
      { value: '21 Years' },
      { value: '22 Years' },
      { value: '23 Years' },
      { value: '24 Years' },
      { value: '25+ Years' }
    ];
  }


  /* function getBorrowerData
      * @purpose: Fetching the borrower data by Id
      * @version: 1.0.1
      * @author: bhanuwhite
   */
  getBorrowerData(url: any) {
    try {
      this.isLoading = true;
      this.userService.get(url).subscribe(resp => {
        this.isLoading = false;
        if (resp && resp['borrower']) {
          this.borrowerInfo = resp['borrower'];
          this.coborrower = resp['coborrower'];
        } else {
          this.borrowerInfo = '';
          this.coborrower = '';
        }
      }, err => {
        this.isLoading = false;
      });
    } catch (excep) {
      this.isLoading = false;
      console.log(excep);
    }
  }

  borrowerInfoSubmit(form: any, isBorrower: boolean) {
    let validationCheck = [];
    Object.keys(form.controls).map(i => {
      if (form.controls[i].status.toLowerCase() === 'invalid') {

        validationCheck.push({ status: form.controls[i].status });
      }
    });

    if (isBorrower) {
      this.validationremaining = validationCheck.length;
      //this.coborrowerValidationRemaining = 0;
    } else {
      //this.validationremaining = 0;
      this.coborrowerValidationRemaining = validationCheck.length;
    }
    this.editForm();
    this.messageEvent.emit(this.borrowerInfo);
  }

  editForm() {
    $(".editable-items").toggleClass("editable-fields");
    $(".editable-items").toggleClass("editable-form");
    $(".saveform").toggleClass("d-inline-block");
  }


  isAddressRequired(isRadioChecked: boolean) {
    let borrowerData = this.borrowerInfo.address.present;
    let coborrowerData = this.coborrower.address.present;
    if (isRadioChecked) {
      borrowerData.mailingAddress = borrowerData.street;
      borrowerData.mailingCity = borrowerData.city;
      borrowerData.mailingPostalCode = borrowerData.postalCode;
      coborrowerData.mailingAddress = coborrowerData.street;
      coborrowerData.mailingCity = coborrowerData.city;
      coborrowerData.mailingPostalCode = coborrowerData.postalCode;
    } else {
      borrowerData.mailingAddress = '';
      borrowerData.mailingCity = '';
      borrowerData.mailingPostalCode = '';
      coborrowerData.mailingAddress = '';
      coborrowerData.mailingCity = '';
      coborrowerData.mailingPostalCode = '';
    }
  }

  borrowerEvent(isBorrower: boolean) {
    // if (isBorrower) {
    //   this.coborrowerValidationRemaining = 0;
    // } else {
    //   this.validationremaining = 0;
    // }
  }
}