import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { User } from '../borrower/models/user';
import { CommonConstants } from 'src/app/config/constants';



@Injectable()
export class UserService {

    // baseUrl = 'assets/data/'; here will be your api link. here i have used demo link ;

    constructor(public http: HttpClient) { }

    getUsers() {
        return this.http.get(CommonConstants.baseUrl + 'users.json');
    }
    addUser(user: User) {
        const url = CommonConstants.baseUrl + '/add';
        return this.http.post(url, user);  // return this.http.post<any>(url, user);
    }
    updateUser(user: User) {
        const url = CommonConstants.baseUrl + '/update/' + user.id;
        const body = user;
        return this.http.post(url, body);
    }
    deleteUser(id: number) {
        const url = CommonConstants.baseUrl + '/delete/' + id;
        // const body = { id: id };
        return this.http.post(url, id);
    }

    // I am here post method to update and delete, But you can use  PUT an DELETE method to update and delete repectively.

    

    /* function post
     * @purpose: post request
     * @version: 1.0.1
     * @author: bhanuwhite
     */

    post(url: string, data: any) {
        return this.http.post(CommonConstants.baseUrl + url, data);
    }

    /* function getAll
       * @purpose: getAll request
       * @version: 1.0.1
       * @author: bhanuwhite
    */

    getAll(url: string) {
        return this.http.get(CommonConstants.baseUrl + url);
    }

    /* function put
       * @purpose: put request
       * @version: 1.0.1
       * @author: bhanuwhite
    */
    put(url: string, id: number, data: any) {
        return this.http.put(CommonConstants.baseUrl + url + id, data);
    }

    /* function delete
       * @purpose: delete request
       * @version: 1.0.1
       * @author: bhanuwhite
    */

    delete() {
        return this.http.delete(CommonConstants.baseUrl);
    }

    /* function get
     * @purpose: for getting the response from API call
     * @version: 1.0.1
     * @author: bhanuwhite
     */
    get(url: string) {
        return this.http.get(CommonConstants.baseUrl + url);
    }
}
