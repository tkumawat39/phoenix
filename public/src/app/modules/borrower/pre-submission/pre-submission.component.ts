import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { Observable, Subscription } from 'rxjs';


import { CancelLoanDialogComponent } from './../../../shared/dialogs/borroer-pipeline/cancel-loan-dialog/cancel-loan-dialog.component';
import { CallContactDialogComponent } from './../../../shared/dialogs/borroer-pipeline/call-contact-dialog/call-contact-dialog.component';
import { PreSubmissionTableModel } from '../models/borrower';

@Component({
  selector: 'app-borrower-pipeline-pre-submission',
  templateUrl: './pre-submission.component.html'
})

export class PreSubmissionComponent implements OnInit {

  /** this code is used for dropdown filter */
  // columnsSortingDDLFlag: any= { 'firstName': false, 'lastName':false, 'salesStage':false,
  // 'loanAmount':false,'loanProgram':false, 'estClosingDate':false, 'prospectDate':false,
  // 'statusDate':false, 'assignedAgent':false, 'state':false, 'takeAction':false};

  displayedColumns: string[] = ['firstName', 'lastName', 'salesStage', 'loanAmount',
    'loanProgram', 'estClosingDate', 'prospectDate', 'statusDate', 'assignedAgent', 'state', 'takeAction'];
  dataSource = new MatTableDataSource<PreSubmissionTableModel>(data);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CancelLoanDialogComponent, {});
    dialogRef.afterClosed().subscribe(result => { });
  }

  openDialogCallContact(): void {
    const dialogRef = this.dialog.open(CallContactDialogComponent, {
      data: { dialogTitle: '', dialogText: '' }
    });
    dialogRef.afterClosed().subscribe(result => { });
  }

  applyFilter(filterValue: any) {
  }

  editData() {
   console.log('edit');
  }

  // applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue;
  // }

  /** this code is used for dropdown filter */
  // applyFilter(filterValue: string, columnName?: string) {
  //   this.dataSource.filterPredicate = (data: any, fitlerString: string) => {
  //     let flag = false;
  //     for (let i = 0; i < this.displayedColumns.length; i++) {
  //       if (columnName == null || this.displayedColumns[i] == columnName) {
  //         flag = false;
  //         let dataString = JSON.stringify(data[this.displayedColumns[i]]);
  //         flag = dataString.toLowerCase().indexOf(fitlerString.trim().toLowerCase()) != -1 ? true : false;
  //         if (flag)
  //           break;
  //       }
  //     }
  //     return flag;
  //   };
  //   this.dataSource.filter = filterValue;
  // }

  // applyAmountFilter(fromFilterValue: string, toFilterValue: string, columnName: string) {
  //   this.dataSource.filterPredicate = (data: any, fitlerString: string) => {
  //     let flag = false;
  //     for (let i = 0; i < this.displayedColumns.length; i++) {
  //       if (this.displayedColumns[i] == columnName) {
  //         let fromValue = parseFloat(fitlerString.split("_")[0]);
  //         let toValue = parseFloat(fitlerString.split("_")[1]);
  //         flag = false;
  //         let dataAmount = parseFloat(JSON.stringify(data[this.displayedColumns[i]]).replace(/[^0-9-]/g, '').toString());
  //         if (dataAmount >= fromValue && dataAmount <= toValue)
  //           flag = true;

  //         if (flag)
  //           break;
  //       }
  //     }
  //     return flag;
  //   };
  //   fromFilterValue = fromFilterValue == '' ? '-99999999999' : fromFilterValue;
  //   toFilterValue = toFilterValue == '' ? '999999999999' : toFilterValue;
  //   this.dataSource.filter = fromFilterValue + "_" + toFilterValue;
  // }


}


const data: PreSubmissionTableModel[] = [
  {
    firstName: 'Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: '1abc ontact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$100',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2050',
    prospectDate: '01/15/2050',
    statusDate: '01/25/2052',
    assignedAgent: [{ name: 'abc Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: true, phone: true, email: false, notes: false, alarm: false }
  },
  {
    firstName: 'Edgar',
    lastName: 'Hebron',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$-300',
    loanProgram: 'Conv 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'AL',
    takeAction: { edit: false, phone: true, email: false, notes: true, alarm: false }
  },
  {
    firstName: 'Catty Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' }, { name: 'Jeff Brown' },
    { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: true, email: false, notes: true, alarm: true }
  },
  {
    firstName: 'Katty Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$200,250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' }, { name: 'Jeff Brown' },
    { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: true, email: true, notes: true, alarm: true }
  },
  {
    firstName: 'Yalk Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$200,250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: true, email: true, notes: true, alarm: true }
  },
  {
    firstName: 'Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$200,250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: true, email: true, notes: true, alarm: true }
  },
  {
    firstName: 'Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$200,250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: true, email: true, notes: true, alarm: true }
  },
  {
    firstName: 'Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$200,250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: false, email: false, notes: false, alarm: true }
  },
  {
    firstName: 'Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$200,250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: true, email: true, notes: true, alarm: true }
  },
  {
    firstName: 'Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$200,250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: true, email: true, notes: true, alarm: true }
  },
  {
    firstName: 'Abby',
    lastName: 'Jenkins',
    salesStage: [{ type: 'Contact Obtained' }, { type: 'Cancel Loan' }],
    loanAmount: '$200,250',
    loanProgram: 'FHA 30Yr Fixed',
    estClosingDate: '06/16/2019',
    prospectDate: '01/15/2018',
    statusDate: '01/25/2019',
    assignedAgent: [{ name: 'Alice Sample-Tester' },
    { name: 'Jeff Brown' }, { name: 'Brian Apunda' }, { name: 'Quang Dangtran' }, { name: 'Michael Bull' }],
    state: 'TX',
    takeAction: { edit: false, phone: true, email: true, notes: true, alarm: true }
  },
];
