import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'app-borrower-pipeline-funded',
  templateUrl: './funded.component.html'
})

export class FundedComponent implements OnInit, OnDestroy {

  sub: Subscription;

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

}
