
export interface PreSubmissionTableModel {
  firstName: string;
  lastName: string;
  salesStage: any;
  loanAmount: string;
  loanProgram: string;
  estClosingDate: string;
  prospectDate: string;
  statusDate: string;
  assignedAgent: any;
  state: string;
  takeAction: any;
}
