import { Component, OnInit, OnDestroy, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import * as $ from 'jquery';
import 'bootstrap-daterangepicker';

@Component({
  selector: 'app-home-todo-opportunities',
  templateUrl: './todo-opportunities.component.html'
})
export class TodoOpportunitiesComponent implements OnInit, OnDestroy, AfterViewInit {

  sub: Subscription;
  // @ViewChild('daterange',{ read: true, static: false }) daterange;

  constructor(private el: ElementRef) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      // console.log((<any>$('.datepicker')).val());
      // (<any>$('input[name="dates"]')).daterangepicker();

    }, 0);



  }

  ngOnDestroy() {
  }


}
