import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'app-home-todo-pipeline',
  templateUrl: './todo-pipeline.component.html'
})
export class TodoPipelineComponent implements OnInit, OnDestroy {

  sub:Subscription;

  constructor() { }
  
  ngOnInit() {   
  }

  ngOnDestroy() {
  }


}
