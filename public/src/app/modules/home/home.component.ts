import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


import * as $ from 'jquery';
import 'bootstrap-daterangepicker';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  sub: Subscription;
  constructor(
    private el: ElementRef,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    // setTimeout(()=>{
    //   $('.dropdown-toggle-custom').on('click', function (e) {
    //     $(this).next().parent().addClass('show');
    //   });
    // },500)
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }

  ngAfterViewInit() {
    // $(this.el.nativeElement).daterangepicker();
  }

  ngOnDestroy() {
  }




}
