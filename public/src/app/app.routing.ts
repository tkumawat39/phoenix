import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PagesComponent } from './pages/pages.component';
import { LoginComponent } from './core/components/auth/login/login.component';
import { AuthComponent } from './core/components/auth/auth.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { OpportunitiesComponent } from './modules/opportunities/opportunities.component';
import { EditComponent } from './modules/opportunities/edit/edit.component';
import { ViewAllComponent } from './modules/borrower/view-all/view-all.component';
import { FundedComponent } from './modules/borrower/funded/funded.component';
import { ClosingComponent } from './modules/borrower/closing/closing.component';
import { UnderwritingComponent } from './modules/borrower/underwriting/underwriting.component';
import { PreSubmissionComponent } from './modules/borrower/pre-submission/pre-submission.component';
import { BorrowerPipelineComponent } from './modules/borrower/borrower-pipeline.component';
import { HomeComponent } from './modules/home/home.component';
import { AuthGuard } from './core/guards/auth.guard';
import { UncontactedComponent } from './modules/opportunities/uncontacted/uncontacted.component';

// /auth/login
export const routes: Routes = [{
    path: '', component: PagesComponent, canActivate: [AuthGuard], children: [
        { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
        { path: 'home', component: HomeComponent },
        {
            path: 'opportunities', component: OpportunitiesComponent, children: [
                { path: '', redirectTo: '/opportunities/un-contacted', pathMatch: 'full' },
                { path: 'un-contacted', component: UncontactedComponent }
            ]
        },
        {
            path: 'opportunities/un-contacted/:id', component: EditComponent
        },
        {
            path: 'borrower-pipeline', component: BorrowerPipelineComponent, children: [
                { path: '', redirectTo: '/borrower-pipeline/pre-submission', pathMatch: 'full' },
                { path: 'pre-submission', component: PreSubmissionComponent },
                { path: 'underwriting', component: UnderwritingComponent },
                { path: 'closing', component: ClosingComponent },
                { path: 'funded', component: FundedComponent },
                { path: 'view-all', component: ViewAllComponent }
            ]
        },
    ]
},
{
    path: 'auth', component: AuthComponent, children: [
        { path: 'login', component: LoginComponent }
    ]
},
{ path: '**', component: NotFoundComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,  // <- comment this line for activate lazy load
    useHash: true
});
